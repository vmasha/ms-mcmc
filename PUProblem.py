from dolfin import *
import petsc4py
from petsc4py import PETSc
import numpy as np

meps = 1.0e-10
def allbound(x, on_boundary):
    return on_boundary
def bX0(x, on_boundary):
    return on_boundary and x[0] < meps
def bX1(x, on_boundary):
    return on_boundary and abs(x[0] - 1.0) < meps
def bY0(x, on_boundary):
    return on_boundary and x[1] < meps
def bY1(x, on_boundary):
    return on_boundary and abs(x[1] - 1.0) < meps
def bZ0(x, on_boundary):
    return on_boundary and x[2] < meps
def bZ1(x, on_boundary):
    return on_boundary and abs(x[2] - 1.0) < meps
class TopBound(SubDomain):
    def inside(self, x, on_boundary):
        return bY0(x, on_boundary)
    
class HeterExpression(Expression):
    def setArr(self, dim, Nx, Ny, Nz, arr):
        self.arr = arr
        self.dim = dim
        self.Nx = Nx
        self.Ny = Ny
        self.Nz = Nz
    def value_shape(self):
        return ()
    def eval_cell(self, value, x, ufc_cell):
        if(self.dim == 2):
            ci = ufc_cell.index/2
            j, i = divmod(ci, self.Nx)
            value[0] = self.arr[int(i),int(j)]
        if(self.dim == 3):
            ci = ufc_cell.index/6
            lj, i = divmod(ci, self.Nx)
            k, j = divmod(lj, self.Ny)
            value[0] = self.arr[int(i),int(j),int(k)]

class PUProblem:
    def __init__(self, dim, mesh, g1, gamma):
        self.mesh = mesh
        self.dim = dim
        self.mesh.init()
        # define space and function
        self.VT = FiniteElement("CG", self.mesh.ufl_cell(), 1)
        self.VU = VectorElement("CG", self.mesh.ufl_cell(), 1)
        self.V = FunctionSpace(self.mesh, self.VT * self.VU)
        (self.p, self.u) = TrialFunctions(self.V)
        (self.q, self.v) = TestFunctions(self.V)
        self.sol = Function(self.V); self.sol.rename('u', '0')
        self.solms = Function(self.V); self.solms.rename('u', '0')
        self.g0 = Constant(0.0)
        self.g1 = g1#Constant(1.0)
        self.gamma = gamma
        # self.bc2 = DirichletBC(self.V.sub(0), self.g1, bY1)
        self.bcUX1 = DirichletBC(self.V.sub(1).sub(0), self.g0, bX0)
        self.bcUY1 = DirichletBC(self.V.sub(1).sub(1), self.g0, bY0)
        if self.dim == 3:
            self.bcUZ1 = DirichletBC(self.V.sub(1).sub(2), self.g0, bZ0)
         # ds
        self.bounds = MeshFunction("size_t", self.mesh, self.dim - 1)
        self.bounds.set_all(0)
        force_boundary = AutoSubDomain(bY1)
        force_boundary.mark(self.bounds, 1)
        print("init forms")

    def form2mat(self, c, dt, k, E, nu, alpha):
        lmd = nu*E/(1+nu)/(1-2*nu)
        mu  = E/2/(1+nu)
        def epsilon(u):
            return 0.5*(grad(u) + grad(u).T)
        def sigma(u):
            return 2*mu*epsilon(u) + lmd*tr(epsilon(u))*Identity(self.dim)
        # form
        ds = Measure('ds', domain=self.mesh, subdomain_data=self.bounds)
        a = inner(sigma(self.u), epsilon(self.v))*dx + alpha*inner(grad(self.p), self.v)*dx\
          + alpha/dt*div(self.u)*self.q*dx + c/dt*self.p*self.q*dx + inner(k*grad(self.p), grad(self.q))*dx\
          + self.gamma*self.p*self.q*ds(1)
        self.A = PETScMatrix()
        assemble(a, tensor = self.A)
        # self.bc2.apply(self.A)
        self.bcUX1.apply(self.A)
        self.bcUY1.apply(self.A)
        if self.dim == 3:
            self.bcUZ1.apply(self.A)
        print("init mat")

    def form2rhs(self, c, dt, u0, alpha):
        (pn, un) = split(u0)
        # form
        ds = Measure('ds', domain=self.mesh, subdomain_data=self.bounds)
        L = c/dt*pn*self.q*dx + alpha/dt*div(un)*self.q*dx\
          + self.gamma*self.g1*self.q*ds(1)
        self.b = PETScVector()
        assemble(L, tensor = self.b)
        # self.bc2.apply(self.b)
        self.bcUX1.apply(self.b)
        self.bcUY1.apply(self.b)
        if self.dim == 3:
            self.bcUZ1.apply(self.b)

    def solve(self):
        solve(self.A,  self.sol.vector(), self.b, "default", "default")


class MsProblem:
    def __init__(self, V):
        self.timer = Timer()
        self.sol = Function(V); self.sol.rename('u', '0')

    def loadR(self, indir, Nw, ecountP, ecountU, bind, incp, incu, incrU):
        self.timer.start()
        ecountPIn = ecountP
        ecountPBc = ecountP + incp
        ecountUIn = ecountU
        ecountUBc = ecountU + incu
        Nb = len(bind)
        print(Nb, ecountPBc, ecountUBc)
        NeP = (Nw-Nb)*ecountPIn + Nb*ecountPBc
        NeU = (Nw-Nb)*ecountUIn + Nb*ecountUBc
        #
        Nf = self.sol.vector().size()
        Ne = NeP + NeU
        self.R = PETSc.Mat().createAIJ([Ne, Nf], nnz=Nf)
        ecounter = 0
        # P
        for wi in range(Nw):
            dofp = np.loadtxt(indir + 'ms-p/dof/w'+str(wi), dtype=int)
            NlocP = len(dofp)
            ecountPcur = ecountPIn
            if wi in bind:
                ecountPcur = ecountPBc
            for ei in range(ecountPcur):
                pbasis = np.loadtxt(indir + 'ms-p/eigen/w'+str(wi)+'-e'+str(ei), dtype=float)
                for di in range(NlocP):
                    self.R.setValue(ecounter, dofp[di], pbasis[di])
                ecounter += 1
        # U
        for wi in range(Nw):
            dofu   = np.loadtxt(indir + 'ms-u/dof/w'+str(wi), dtype=int)
            NlocU = len(dofu)
            ecountUcur = ecountUIn
            if wi in bind:
                ecountUcur = ecountUBc
            for ei in range(ecountUcur):
                ubasis = np.loadtxt(indir + 'ms-u/eigen/w'+str(wi)+'-e'+str(ei), dtype=float)
                for di in range(NlocU):
                    dofi = incrU + dofu[di]
                    self.R.setValue(ecounter, dofi, ubasis[di])
                ecounter += 1
        self.R.assemblyBegin() 
        self.R.assemblyEnd()
        
        self.bne = PETSc.Vec().createSeq(Ne)
        self.xne = PETSc.Vec().createSeq(Ne)
        self.rhsc = PETScVector(self.bne)
        self.xc = PETScVector(self.xne) 
        self.ums = PETSc.Vec().createSeq(Nf)
        print( "load R[%d, %d] , Time = %g sec" % (ecounter, Nf, self.timer.stop()) )
        
    def projectRART(self, A):
        self.timer.start()
        RAf = self.R.matMult(A.mat())
        self.Ac2 = RAf.matTransposeMult(self.R)
        self.Ac = PETScMatrix(self.Ac2)
        print("RART, Time = %g sec" % self.timer.stop())
  
    def projectRb(self, b):
        self.R.mult(b.vec(), self.rhsc.vec())
#         print("MS generate bc")#, self.rhsc.array())
        
    def solveMs(self): 
        solve(self.Ac, self.xc, self.rhsc)
#         print('MS solve u in ' + str(self.xc.min()) + ', ' + str(self.xc.max()) ) 
        self.R.multTranspose(self.xc.vec(), self.ums)
        self.sol.vector()[:] = self.ums.getArray()
    
