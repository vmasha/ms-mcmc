import numpy as np
import math, argparse, sys
from dolfin import *
sys.path.append('./')
from PUProblem import *
petsc4py.init(sys.argv)
parameters["reorder_dofs_serial"] = False;
parameters["allow_extrapolation"] = True;
timer = Timer()


# python3 solver-ms.py -d 2 -n 1
# python3 solver-ms.py -d 3 -n 1
parser = argparse.ArgumentParser()
parser.add_argument('-d', type=int, required=True,  help="dim")
parser.add_argument('-n', type=int, required=True,  help="realizaton number (k and E)")
parser.add_argument('-b', type=int, required=True,  help="basis count")
parser.add_argument('-g', type=int, required=True,  help="gamma for Robyn bc")
args = parser.parse_args()

dim = args.d
lind = args.n
bcount = args.b
gamma = args.g
print("Solve MS poroelastic on coarse grid, for %d realization (%dd problem)" % (lind, dim) )


bcountp = 1 + bcount
bcountu = dim + bcount

if(dim == 2):
    indir = './data-2d/'
    Nx, Ny, Nz = 100, 100, 1
    Ncx, Ncy, Ncz = 10, 10, 1
    tmax = 0.001
if(dim == 3):
    indir = './data-3d/'
    Nx, Ny, Nz = 20, 20, 20
    Ncx, Ncy, Ncz = 5, 5, 5
    tmax = 0.001

Ncv = (Ncx+1)*(Ncy+1)
if dim == 3:
    Ncv = (Ncx+1)*(Ncy+1)*(Ncz+1)
print(Ncv)

tcount = 20 
dt = tmax/tcount
alpha = 0.1
c = 1.0
nu = 0.3
g1 = 1.0
print("Tmax = %g with %d time steps" % (tmax, tcount))

# =========================
# ===== LOAD K and E ======
# =========================
my_data0 = np.loadtxt(indir+'kle/m-'+str(lind), dtype=float)
my_dataK = np.loadtxt(indir+'kle/k-'+str(lind), dtype=float)
my_dataE = np.loadtxt(indir+'kle/E-'+str(lind), dtype=float)
if(dim == 2):
    arr0 = np.reshape(my_data0, (Nx, Ny))
    arrK = np.reshape(my_dataK, (Nx, Ny))
    arrE = np.reshape(my_dataE, (Nx, Ny))
if(dim == 3):
    arr0 = np.reshape(my_data0, (Nx, Ny, Nz))
    arrK = np.reshape(my_dataK, (Nx, Ny, Nz))
    arrE = np.reshape(my_dataE, (Nx, Ny, Nz)) 
print('phi:', arr0.min(), arr0.max())
print('K:', arrK.min(), arrK.max())
print('E:', arrE.min(), arrE.max())
print(arrK.shape)  

# =================
# ===== MESH ======
# =================
if dim == 2:
    fimesh = UnitSquareMesh(Nx, Ny)
if dim == 3:
    fimesh = UnitCubeMesh(Nx, Ny, Nz)
Nvert = fimesh.num_vertices()
print('mesh')

E = HeterExpression(degree = 0)
E.setArr(dim, Nx, Ny, Nz, arrE)
k = HeterExpression(degree = 0)
k.setArr(dim, Nx, Ny, Nz, arrK)

filek = File("./results/k.pvd")
fileE = File("./results/E.pvd")
filep = File("./results/p.pvd")
fileu = File("./results/u.pvd")
filep2 = File("./results/p2.pvd")
fileu2 = File("./results/u2.pvd")
  
# save coeff
V0 = FunctionSpace(fimesh, "DG", 0)
u0 = Function(V0); u0.rename('u', '0')
u0.interpolate(k)
filek << u0; filek << u0 
print('k(%g, %g)' % (u0.vector().min(), u0.vector().max()))
u0.interpolate(E)
fileE << u0; fileE << u0
print('E(%g, %g)' % (u0.vector().min(), u0.vector().max()))

if dim == 2:
    bind = [110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120]
if dim == 3:
    bind = [30, 31, 32, 33, 34, 35,
            66, 67, 68, 69, 70, 71,
            102, 103, 104, 105, 106, 107,
            138, 139, 140, 141, 142, 143,
            174, 175, 176, 177, 178, 179,
            210, 211, 212, 213, 214, 215]

# ====================
# ===== PROBLEM ======
# ====================
problemF = PUProblem(dim, fimesh, g1, gamma)
problemF.form2mat(c, dt, k, E, nu, alpha)

problemMs = MsProblem(problemF.V)
problemMs.loadR(indir, Ncv, bcountp, bcountu, bind, 0, 0, Nvert)
problemMs.projectRART(problemF.A)

# initial
if(dim == 2):
    problemF.sol.interpolate( Constant((0.0, 0.0, 0.0)) )
    problemMs.sol.interpolate( Constant((0.0, 0.0, 0.0)) )
if(dim == 3):
    problemF.sol.interpolate( Constant((0.0, 0.0, 0.0, 0.0)) )
    problemMs.sol.interpolate( Constant((0.0, 0.0, 0.0, 0.0)) )

# ==================
# ===== SOLVE ======
# ==================
timer.start()
t = 0; tcounter = 0
while t < tmax:
    
    # fine
    un = problemF.sol.copy(deepcopy=True)
    problemF.form2rhs(c, dt, un, alpha)
    problemF.solve()
    (pp, uu) = problemF.sol.split()
    filep << pp
    fileu << uu
    print('Time[%d] = %f: u(%g, %g)' % (tcounter, t, problemF.sol.vector().min(), problemF.sol.vector().max()))
     
    # ms
    ums0 = problemMs.sol.copy(deepcopy=True)
    problemF.form2rhs(c, dt, ums0, alpha)
    problemMs.projectRb(problemF.b)
    problemMs.solveMs()
    (pp, uu) = problemMs.sol.split()
    filep2 << pp
    fileu2 << uu
    print('MS u(%g, %g)' % (problemMs.sol.vector().min(), problemMs.sol.vector().max()))
    
    # error
    arrMs = np.array(problemMs.sol.vector())
    arrEx = np.array(problemF.sol.vector())
    errp = 0.0; sump = 0.0
    for i in range(Nvert):
        errp += math.pow(arrMs[i] - arrEx[i], 2)
        sump += math.pow(arrEx[i], 2)
    erru = 0.0; sumu = 0.0
    for i in range(Nvert, len(arrEx)):
        erru += math.pow(arrMs[i] - arrEx[i], 2)
        sumu += math.pow(arrEx[i], 2)
    print('---> ERROR_p = %g, ERROR_u = %g' % (math.sqrt(errp/sump)*100, math.sqrt(erru/sumu)*100) )
    
    t += dt
    tcounter += 1
print(timer.stop())

