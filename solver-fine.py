import numpy as np
import math, argparse, sys
from dolfin import *
sys.path.append('./')
from PUProblem import *
parameters["reorder_dofs_serial"] = False;
parameters["allow_extrapolation"] = True;
timer = Timer()


# python3 solver-fine.py -d 2 -n 15
# python3 solver-fine.py -d 3 -n 7
parser = argparse.ArgumentParser()
parser.add_argument('-d', type=int, required=True,  help="dim")
parser.add_argument('-n', type=int, required=True,  help="realizaton number (k and E)")
parser.add_argument('-g', type=int, required=True,  help="gamma for Robyn bc")
args = parser.parse_args()

dim = args.d
lind = args.n
gamma = args.g
print("Solve poroelastic on fine grid, for %d realization (%dd problem)" % (lind, dim) )


if(dim == 2):
    kledir = './data-2d/kle/'
    Nx, Ny, Nz = 100, 100, 1
    tmax = 0.001
if(dim == 3):
    kledir = './data-3d/kle/'
    Nx, Ny, Nz = 20, 20, 20
    tmax = 0.001
N = Nx*Ny*Nz
print(N)

tcount = 20 
dt = tmax/tcount
alpha = 0.1
c = 1.0
nu = 0.3
g1 = 1.0
print("Tmax = %g with %d time steps" % (tmax, tcount))

# =========================
# ===== LOAD K and E ======
# =========================
my_data0 = np.loadtxt(kledir+'m-'+str(lind), dtype=float)
my_dataK = np.loadtxt(kledir+'k-'+str(lind), dtype=float)
my_dataE = np.loadtxt(kledir+'E-'+str(lind), dtype=float)

if(dim == 2):
    arr0 = np.reshape(my_data0, (Nx, Ny))
    arrK = np.reshape(my_dataK, (Nx, Ny))
    arrE = np.reshape(my_dataE, (Nx, Ny))
if(dim == 3):
    arr0 = np.reshape(my_data0, (Nx, Ny, Nz))
    arrK = np.reshape(my_dataK, (Nx, Ny, Nz))
    arrE = np.reshape(my_dataE, (Nx, Ny, Nz))
        
print('phi:', arr0.min(), arr0.max())
print('K:', arrK.min(), arrK.max())
print('E:', arrE.min(), arrE.max())
print(arrK.shape)

# =================
# ===== MESH ======
# =================
if dim == 2:
    fimesh = UnitSquareMesh(Nx, Ny)
if dim == 3:
    fimesh = UnitCubeMesh(Nx, Ny, Nz)
print('mesh')

# ====================
# ===== PROBLEM ======
# ====================
E = HeterExpression(degree = 0)
E.setArr(dim, Nx, Ny, Nz, arrE)
k = HeterExpression(degree = 0)
k.setArr(dim, Nx, Ny, Nz, arrK)

filek = File("./results/k.pvd")
fileE = File("./results/E.pvd")
filep = File("./results/p.pvd")
fileu = File("./results/u.pvd")
   
# init problem
problem = PUProblem(dim, fimesh, g1, gamma)
problem.form2mat(c, dt, k, E, nu, alpha)
# initial
if(dim == 2):
    problem.sol.interpolate( Constant((0.0, 0.0, 0.0)) )
if(dim == 3):
    problem.sol.interpolate( Constant((0.0, 0.0, 0.0, 0.0)) )

# save coeff
V0 = FunctionSpace(problem.mesh, "DG", 0)
u0 = Function(V0); u0.rename('u', '0')
u0.interpolate(k)
filek << u0; filek << u0 
print('k(%g, %g)' % (u0.vector().min(), u0.vector().max()))
u0.interpolate(E)
fileE << u0; fileE << u0
print('E(%g, %g)' % (u0.vector().min(), u0.vector().max()))

# ==================
# ===== SOLVE ======
# ==================
timer.start()
t = 0; tcounter = 0
while t <= tmax:
    un = problem.sol.copy(deepcopy=True)
    problem.form2rhs(c, dt, un, alpha)
    problem.solve()
    print('Time[%d] = %f: u(%g, %g)' % (tcounter, t, problem.sol.vector().min(), problem.sol.vector().max()))
    
    (pp, uu) = problem.sol.split()
    filep << pp
    fileu << uu
    
    t += dt
    tcounter += 1

print("Solution time %g sec" % timer.stop())
