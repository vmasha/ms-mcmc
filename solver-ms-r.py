import math, argparse, sys
from random import random, seed
import numpy as np
from dolfin import *
sys.path.append('./')
from PUProblem import *
petsc4py.init(sys.argv)
parameters["reorder_dofs_serial"] = False;
parameters["allow_extrapolation"] = True;
timer = Timer()
timerGl = Timer()
seed(961)

# python solver-mcmc.py -d 2 -n 7 -g 10000 -t 2 -m 100 -o ./results-t2/
# python solver-mcmc.py -d 2 -n 7 -g 10000 -t 5 -m 100 -o ./results-t2-ML/

parser = argparse.ArgumentParser()
parser.add_argument('-d', type=int, required=True,  help="dim")
parser.add_argument('-n', type=int, required=True,  help="realizaton number (k and E)")
parser.add_argument('-g', type=int, required=True,  help="gamma for Robyn bc")
parser.add_argument('-t', type=int, required=True,  help="type: 1 - check ms for exact (lind = n), 2 - check F_obs, 3 - two-stage mcmc, 4 - one-stage mcmc")
parser.add_argument('-m', type=int, required=True,  help="number of random fields")
parser.add_argument('-o', dest='outdir',  help="output folder")

args = parser.parse_args()

dim = args.d
lind = args.n
gamma = args.g

mtype = args.t
mnum = args.m

outdir = args.outdir

print("MCMC poroelastic on coarse grid, for %d realization (%dd problem ) out=%s" % (lind, dim, outdir) )

if(dim == 2):
    indir = './data-2d/'
    Nx, Ny, Nz = 100, 100, 1
    Ncx, Ncy, Ncz = 10, 10, 1
    tmax = 0.001
if(dim == 3):
    indir = './data-3d/'
    Nx, Ny, Nz = 20, 20, 20
    Ncx, Ncy, Ncz = 5, 5, 5
    tmax = 0.001

tcount = 20 
dt = tmax/tcount

alpha = 0.1
c = 1.0
nu = 0.3
g1 = 1.0
print("Tmax = %g with %d time steps" % (tmax, tcount))

Ncv = (Ncx+1)*(Ncy+1)
if dim ==3:
    Ncv = (Ncx+1)*(Ncy+1)*(Ncz+1)
print(Ncv)

Nfc = Nx*Ny
if dim == 3:
    Nfc = Nx*Ny*Nz
print(Nfc)

# number of basis
bcc = []
if mtype == 1:
    bcc = [0, 1, 2, 3, 4, 6, 8]
if mtype == 2:
    bcc = [0, 1, 2, 4, 8]

# ===========================
# ===== LOAD EXACT k,E ======
# ===========================
my_dataK = np.loadtxt(indir+'kle/k-'+str(lind), dtype=float)
my_dataE = np.loadtxt(indir+'kle/E-'+str(lind), dtype=float)
if(dim == 2):
    arrK0 = np.reshape(my_dataK, (Nx, Ny))
    arrE0 = np.reshape(my_dataE, (Nx, Ny))
if(dim == 3):
    arrK0 = np.reshape(my_dataK, (Nx, Ny, Nz))
    arrE0 = np.reshape(my_dataE, (Nx, Ny, Nz)) 
print('K:', arrK0.min(), arrK0.max())
print('E:', arrE0.min(), arrE0.max())
print(arrK0.shape)  

# =====================
# ===== LOAD KLE ======
# =====================
minphi = 0.05
maxphi = 0.2

klecount = 200
klebases = np.zeros( (klecount, Nfc) )
for ki in range(klecount):
    infile0 = indir + 'kle/basis-' + str(ki)
    with open(infile0) as f:
        lines_list = f.readlines()
        j = 0
        for val in lines_list[1::]:
            klebases[ki, j] = float(val)
            j += 1
print(len(klebases), klecount)

def generateKLE(rcoefs):
    my_data0 = np.matmul(rcoefs, klebases)
    # my_data0 = np.zeros(Nfc)
    # for ki in range(klecount):
    #     for i in range(Nfc):
    #         my_data0[i] += rcoefs[ki] * klebases[ki][i]
    # rescale and find porosity
    my_data0[:] = (my_data0[:] - my_data0.min())/(my_data0.max() - my_data0.min())
    my_data0[:] = my_data0[:]*(maxphi - minphi) + minphi
    print('phi:', my_data0.min(), my_data0.max())
    return my_data0

# generate k and E
def generateK(my_data0):
    my_dataK = np.zeros(Nfc)
    for i in range(Nfc):
        val = my_data0[i]
        my_dataK[i] = math.exp(40*val)
    print('K:',   my_dataK.min(), my_dataK.max())
    if dim == 2:
        arrK = np.reshape(my_dataK, (Nx, Ny))
    if dim == 3:
        arrK = np.reshape(my_dataK, (Nx, Ny, Nz))
    return arrK

def generateE(my_data0):        
    my_dataE = np.zeros(Nfc)
    for i in range(Nfc):
        val = my_data0[i]
        my_dataE[i] = 0.1*math.pow((1.0 - val)/val, 1.5)
    print('E:',   my_dataE.min(), my_dataE.max())
    if dim == 2:
        arrE = np.reshape(my_dataE, (Nx, Ny))
    if dim == 3:
        arrE = np.reshape(my_dataE, (Nx, Ny, Nz))
    return arrE

def getVIndex(i, j, k, Nx, Ny):
    if dim == 3:
        return (k*(Ny+1) + j)*(Nx+1) + i
    else:
        return j*(Nx+1) + i
    
# observation data
def findF(sol):
    arrsol = np.array(sol.vector())
    if dim == 2:
        Nv1 = (Nx+1)*(Ny+1)
        Nv2 = Nv1*2
        obsdata = np.zeros( (Nx+1)*dim )
        icount = 0
        for i in range(Nx+1):
            vi = getVIndex(i, Ny, 0, Nx, Ny) + Nv1
            obsdata[icount] = arrsol[vi]; icount += 1
            vi = getVIndex(i, Ny, 0, Nx, Ny) + Nv2
            obsdata[icount] = arrsol[vi]; icount += 1
    if dim == 3:
        Nv1 = (Nx+1)*(Ny+1)*(Nz+1)
        Nv2 = Nv1*2
        Nv3 = Nv1*3
        obsdata = np.zeros( (Nx+1)*(Nz+1)*dim )
        icount = 0
        for i in range(Nx+1):
            for j in range(Nz+1):
                vi = getVIndex(i, Ny, j, Nx, Ny) + Nv1
                obsdata[icount] = arrsol[vi]; icount += 1
                vi = getVIndex(i, Ny, j, Nx, Ny) + Nv2
                obsdata[icount] = arrsol[vi]; icount += 1
                vi = getVIndex(i, Ny, j, Nx, Ny) + Nv3
                obsdata[icount] = arrsol[vi]; icount += 1
    return obsdata

def calcerror(arrMs, arrEx, Nvert):
    errp = 0.0; sump = 0.0
    for i in range(Nvert):
        errp += math.pow(arrMs[i] - arrEx[i], 2)
        sump += math.pow(arrEx[i], 2)
    erru = 0.0; sumu = 0.0
    for i in range(Nvert, Nvert*(1+dim)):
        erru += math.pow(arrMs[i] - arrEx[i], 2)
        sumu += math.pow(arrEx[i], 2)
    print('---> ERROR_p = %g, ERROR_u = %g' % (math.sqrt(errp/sump)*100, math.sqrt(erru/sumu)*100) )
    

def solveProblem(isms, problemF, problemMs, k, E, nu, c, dt, alpha):
    timer.start()
    if isms:
        problemMs.projectRART(problemF.A)
    # initial
    if(dim == 2):
        problemF.sol.interpolate( Constant((0.0, 0.0, 0.0)) )
        problemMs.sol.interpolate( Constant((0.0, 0.0, 0.0)) )
    if(dim == 3):
        problemF.sol.interpolate( Constant((0.0, 0.0, 0.0, 0.0)) )
        problemMs.sol.interpolate( Constant((0.0, 0.0, 0.0, 0.0)) )
    # solve
    t = 0; tcounter = 0
    while t < tmax:
        if isms:
            ums0 = problemMs.sol.copy(deepcopy=True)
            problemF.form2rhs(c, dt, ums0, alpha)
            problemMs.projectRb(problemF.b)
            problemMs.solveMs()
            cursol = problemMs.sol
        else: # fine
            un = problemF.sol.copy(deepcopy=True)
            problemF.form2rhs(c, dt, un, alpha)
            problemF.solve()
            cursol = problemF.sol 
        # print('Time[%d] = %f: u(%g, %g)' % (tcounter, t, cursol.vector().min(), cursol.vector().max()))
        t += dt
        tcounter += 1
    print('solution time = %g sec, u(%g, %g)' % (timer.stop(), cursol.vector().min(), cursol.vector().max()) )
    return cursol.copy(deepcopy=True)

bind = []

# =================
# ===== MESH ======
# =================
if dim == 2:
    fimesh = UnitSquareMesh(Nx, Ny)
if dim == 3:
    fimesh = UnitCubeMesh(Nx, Ny, Nz)
Nvert = fimesh.num_vertices()
print('mesh')

V0 = FunctionSpace(fimesh, "DG", 0)
u0 = Function(V0); u0.rename('u', '0')

# =====================
# ===== PROBLEMS ======
# =====================
problemF = PUProblem(dim, fimesh, g1, gamma)
allMsProblems = []
for bi in bcc:
    bcountp = 1 + bi
    bcountu = dim + bi
    problemMs = MsProblem(problemF.V)
    problemMs.loadR(indir, Ncv, bcountp, bcountu, bind, 0, 0, Nvert)
    allMsProblems.append(problemMs)

# ==================
# ===== EXACT ======
# ==================
E = HeterExpression(degree = 0)
E.setArr(dim, Nx, Ny, Nz, arrE0)
k = HeterExpression(degree = 0)
k.setArr(dim, Nx, Ny, Nz, arrK0)

timerGl.start()
problemF.form2mat(c, dt, k, E, nu, alpha)
print("init mat Time %g sec" % timerGl.stop())
solf = solveProblem(False, problemF, problemF, k, E, nu, c, dt, alpha); solf.rename('u', '0') 
arrEx = np.array(solf.vector())
Fobs = findF(solf)
print(len(Fobs))
# print(Fobs)

filep = File(outdir + 'p.pvd')
fileu = File(outdir + 'u.pvd')
fileK = File(outdir + 'k.pvd')
fileE = File(outdir + 'E.pvd')

# ===========================
# ===== MS CHECK EXACT ======
# ===========================
if mtype == 1:
    # save
    u0.interpolate(k); fileK << u0
    u0.interpolate(E); fileE << u0
    (pp, uu) = solf.split()
    filep << pp
    fileu << uu

    for si in range(len(allMsProblems)):
        print('=== MS for +%d ===' % bcc[si])
        solms = solveProblem(True, problemF, allMsProblems[si], k, E, nu, c, dt, alpha); solms.rename('u', '0')
        (pp, uu) = solms.split()
        filep << pp
        fileu << uu
        # error
        calcerror(np.array(solms.vector()), arrEx, Nvert)
        # error for u_top
        Fms = findF(solms)
        errut = 0.0; sumut = 0.0
        for i in range(len(Fobs)):
            errut += math.pow(Fms[i] - Fobs[i], 2)
            sumut += math.pow(Fobs[i], 2)
        err = math.sqrt(errut/sumut)*100
        print('---> ERROR u_top = %g' % err)   

# ===========================
# ===== MS CHECK F_obs ======
# ===========================
if mtype == 2:
    filep2 = File(outdir + "p2.pvd")
    fileu2 = File(outdir + "u2.pvd")

    Nms = len(allMsProblems)
    Fnorms  = np.zeros((mnum, Nms+1))
    ErrsP  = np.zeros((mnum, Nms))
    ErrsU  = np.zeros((mnum, Nms))

    rcoefs = np.zeros(klecount)
    for mi in range(mnum):
        print('----- %d -----' % mi)
        # random field
        for ki in range(klecount):
            rcoefs[ki] = np.random.normal(0.0, 1.0)
        theta = generateKLE(rcoefs)
        arrK = generateK(theta)
        arrE = generateE(theta)
        E = HeterExpression(degree = 0)
        E.setArr(dim, Nx, Ny, Nz, arrE)
        k = HeterExpression(degree = 0)
        k.setArr(dim, Nx, Ny, Nz, arrK)

        # solve fine
        problemF.form2mat(c, dt, k, E, nu, alpha)
        solf = solveProblem(False, problemF, problemF, k, E, nu, c, dt, alpha); solf.rename('u', '0')  
        arrEx = np.array(solf.vector())

        Fprop = findF(solf)
        errut = 0.0; sumut = 0.0
        for i in range(len(Fobs)):
            errut += math.pow(Fprop[i] - Fobs[i], 2)
            sumut += math.pow(Fobs[i], 2)
        Fnorms[mi, 0] = errut/sumut
        print('---> normF = %g' % Fnorms[mi, 0] )  
        
        # save
        np.savetxt(outdir + "m-r"+str(mi)+".txt", theta, fmt='%g')
        np.savetxt(outdir + "k-r"+str(mi)+".txt", arrK, fmt='%g')
        np.savetxt(outdir + "E-r"+str(mi)+".txt", arrE, fmt='%g')
        np.savetxt(outdir + "uf-r"+str(mi)+".txt", Fprop, fmt='%g')


        # solve ms
        for si in range(Nms):
            solms = solveProblem(True, problemF, allMsProblems[si], k, E, nu, c, dt, alpha); solms.rename('u', '0') 
            Fprop = findF(solms)
            errut = 0.0; sumut = 0.0
            for i in range(len(Fobs)):
                errut += math.pow(Fprop[i] - Fobs[i], 2)
                sumut += math.pow(Fobs[i], 2)
            Fnorms[mi, si+1] = errut/sumut
            print('---> %d normMs = %g' % (bcc[si], Fnorms[mi, si+1]) )   
            # error
            # calcerror(np.array(solms.vector()), arrEx, Nvert)
            arrMs = solms.vector()
            errp = 0.0; sump = 0.0
            for i in range(Nvert):
                errp += math.pow(arrMs[i] - arrEx[i], 2)
                sump += math.pow(arrEx[i], 2)
            erru = 0.0; sumu = 0.0
            for i in range(Nvert, Nvert*(1+dim)):
                erru += math.pow(arrMs[i] - arrEx[i], 2)
                sumu += math.pow(arrEx[i], 2)
            ErrsP[mi, si] =  math.sqrt(errp/sump)*100
            ErrsU[mi, si] =  math.sqrt(erru/sumu)*100
            print('---> ERROR_p = %g, ERROR_u = %g' % (math.sqrt(errp/sump)*100, math.sqrt(erru/sumu)*100) )

            np.savetxt(outdir + "ums"+str(bcc[si])+"-r"+str(mi)+".txt", Fprop, fmt='%g')

        # save
        # u0.interpolate(k); fileK << u0
        # u0.interpolate(E); fileE << u0
        # save 
        # (pp, uu) = solf.split()
        # filep << pp
        # fileu << uu
        # (pp, uu) = solms.split()
        # filep2 << pp
        # fileu2 << uu
    # save
    np.savetxt(outdir + "t2-ms-F.txt", Fnorms, fmt='%g')
    np.savetxt(outdir + "t2-ms-F-eu.txt", ErrsU, fmt='%g')
    np.savetxt(outdir + "t2-ms-F-ep.txt", ErrsP, fmt='%g')

# ===========================
# ======== KERAS ML =========
# ===== MS CHECK F_obs ======
# ===========================
if mtype == 5:
    bcount = 4
    Fnorms  = np.zeros((mnum, 3))
    ErrsUtop  = np.zeros( (mnum, 3) )

    # load ml
    from keras import backend as K
    from keras.optimizers import *
    from keras.models import Model, load_model
    def rmse(y_true, y_pred):
        return K.sqrt(K.mean(K.square(y_pred - y_true), axis=-1))/K.sqrt(K.mean(K.square(y_true), axis=-1))*100.0
    def mae2(y_true, y_pred):
        return K.sum(K.abs(y_pred - y_true), axis=-1)/K.sum(K.abs(y_true), axis=-1)*100.0

    xmin = minphi; xmax = maxphi 
    if dim == 2:
        netname = './ml/model2d'
        vmin = [-0.00671568, -0.0477921]
        vmax = [ 0.0223867,  -0.0088949 ]
    if dim == 3:
        netname = './ml/model3d'
        vmin = [-0.00958047, -0.0486761, -0.00644912]  
        vmax = [ 0.0171148,  -0.00976819, 0.0183037]
    models = []
    model1 = load_model(netname + '-0.net', custom_objects={'rmse': rmse, 'mae2':mae2})
    models.append(model1)
    model2 = load_model(netname + '-1.net', custom_objects={'rmse': rmse, 'mae2':mae2})
    models.append(model2)
    if dim == 3:
        model3 = load_model(netname + '-2.net', custom_objects={'rmse': rmse, 'mae2':mae2})
        models.append(model3)
    print('load ML-models')

    if dim == 2:
        FpropMl = np.zeros( (Nx+1)*dim )
    if dim == 3:
        FpropMl = np.zeros( (Nx+1)*(Nz+1)*dim )

    for mi in range(mnum):
        print('----- %d -----' % mi)
        # load random field
        theta = np.loadtxt(outdir + "m-r"+str(mi)+".txt", dtype=float)
        
        # load fine
        Fprop = np.loadtxt(outdir + "uf-r"+str(mi)+".txt", dtype=float)
        errut = 0.0; sumut = 0.0
        for i in range(len(Fobs)):
            errut += math.pow(Fprop[i] - Fobs[i], 2)
            sumut += math.pow(Fobs[i], 2)
        Fnorms[mi, 0] = errut/sumut

        # load ms
        FpropMs = np.loadtxt(outdir + "ums"+str(bcount)+"-r"+str(mi)+".txt", dtype=float)
        errut = 0.0; sumut = 0.0
        for i in range(len(Fobs)):
            errut += math.pow(FpropMs[i] - Fobs[i], 2)
            sumut += math.pow(Fobs[i], 2)
        Fnorms[mi, 1] = errut/sumut

        # input ML
        X_train1 = (theta[:] - xmin) / (xmax - xmin)
        if dim == 2:
            X_train1 = X_train1.reshape(1, Nx, Ny, 1)
        if dim == 3:
            X_train1 = X_train1.reshape(1, Nx, Ny, Nz, 1)
        # predict ML
        for tj in range(dim):
            probs = models[tj].predict(X_train1)
            Fft = probs[0]*(vmax[tj] - vmin[tj]) + vmin[tj]
            # fill Fobs
            if dim == 2:
                for ii in range(Nx+1):
                    FpropMl[dim*ii + tj] = Fft[ii]
            if dim == 3:
                for ii in range(Nx+1):
                    for jj in range(Nz+1):
                        ind = jj*(Nx+1) + ii
                        FpropMl[dim*ind + tj] = Fft[ind]

        # error
        errut = 0.0; sumut = 0.0
        for i in range(len(Fobs)):
            errut += math.pow(FpropMl[i] - Fobs[i], 2)
            sumut += math.pow(Fobs[i], 2)
        Fnorms[mi, 2] = errut/sumut
        

        errut = 0.0; sumut = 0.0
        for i in range(len(Fobs)):
            errut += math.pow(FpropMs[i] - Fprop[i], 2)
            sumut += math.pow(Fprop[i], 2)
        ErrsUtop[mi, 0] =  math.sqrt(errut/sumut)*100
        errut = 0.0; sumut = 0.0
        for i in range(len(Fobs)):
            errut += math.pow(FpropMl[i] - Fprop[i], 2)
            sumut += math.pow(Fprop[i], 2)
        ErrsUtop[mi, 1] =  math.sqrt(errut/sumut)*100
        errut = 0.0; sumut = 0.0
        for i in range(len(Fobs)):
            errut += math.pow(FpropMl[i] - FpropMs[i], 2)
            sumut += math.pow(FpropMs[i], 2)
        ErrsUtop[mi, 2] =  math.sqrt(errut/sumut)*100
        print('---> normF = %g, normMs = %g, normML = %g' % (Fnorms[mi, 0], Fnorms[mi, 1], Fnorms[mi, 2]) ) 
        print('---> ERROR_utop: (ms-f) = %g, (ml-f) = %g, (ml-ms) = %g' % (ErrsUtop[mi, 0], ErrsUtop[mi, 1], ErrsUtop[mi, 2]) )   
        
    # save
    np.savetxt(outdir + "t2-ms-F"+str(lind)+".txt", Fnorms, fmt='%g')
    np.savetxt(outdir + "t2-ms-F-etop"+str(lind)+".txt", ErrsUtop, fmt='%g')


print("DONE")