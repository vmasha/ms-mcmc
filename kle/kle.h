#ifndef KLEXPANSION_H
#define KLEXPANSION_H

#include "quadmesh.h"
#include "./space/Pressure2d.h"
#include "./space/Pressure3d.h"
#include <dolfin.h>
#include <math.h>
#include <iostream>
#include <fstream> 

using namespace dolfin;

double covarianceR2d(int type, 
double x1, double y1, 
double x2, double y2, 
double lx, double ly, double sigma2){
    double rr;
    if(type == 1){
        double norm2 = pow((x1 - x2)/lx, 2) + pow((y1 - y2)/ly, 2);
        rr = sigma2 * exp( -std::sqrt(norm2) );// exponential
    }else{
        double dd = fabs(x1 - x2)/lx + fabs(y1 - y2)/ly;
        rr = sigma2 * exp( -dd );// gaussian
    }
    return rr;
} 

double covarianceR3d(int type, 
double x1, double y1, double z1, 
double x2, double y2, double z2, 
double lx, double ly, double lz, double sigma2){
    double rr;
    if(type == 1){
        double norm2 = pow((x1 - x2)/lx, 2) + pow((y1 - y2)/ly, 2) + pow((z1 - z2)/lz, 2);
        rr = sigma2 * exp( -std::sqrt(norm2) );// exponential
    }else{
        double dd = fabs(x1 - x2)/lx + fabs(y1 - y2)/ly + fabs(z1 - z2)/lz;
        rr = sigma2 * exp( -dd );// gaussian
    }
    return rr;
} 
 
void kle2d(double Lx, double Ly, double Lz, int Nx, int Ny, int Nz, 
int covtype, double lx, double ly, double sigma2, int numEigens, std::string fileNameOut){
    Timer timer("SOLUTION");
    timer.start();

    double dx = lx/Nx;
    double dy = ly/Ny;

    Mesh dmesh; 
    build(dmesh, Nx, Ny, 0, Lx, 0, Ly);
    auto mesh = std::make_shared<Mesh>(dmesh);

    auto Ve = std::make_shared<Pressure2d::FunctionSpace>(mesh);
    Function evec(Ve);
    // File efile(efilename);

    int dN = mesh->num_cells();
    info("generate covariance matrix R[%d, %d]", dN, dN);
    
    Mat CovR;
    MatCreateSeqAIJ(PETSC_COMM_SELF, dN, dN, dN, 0, &CovR);
    for(int i = 0; i < dN; i++){
        Cell v1(*mesh, i);
        double x1 = v1.midpoint().x();
        double y1 = v1.midpoint().y();
        for(int j = 0; j < dN; j++){
            Cell v2(*mesh, j);
            double x2 = v2.midpoint().x();
            double y2 = v2.midpoint().y();
            double val = covarianceR2d(covtype, x1, y1, x2, y2, lx, ly, sigma2)*dx*dy; 
            MatSetValues(CovR, 1, &i, 1, &j ,&val, INSERT_VALUES);
        }
    }
    MatAssemblyBegin(CovR, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(CovR, MAT_FINAL_ASSEMBLY);
    
    auto RR = std::make_shared<PETScMatrix>(CovR);
    if(RR->size(0) < numEigens)
        numEigens = RR->size(0);

    info("RR Time=%g", timer.stop() );
    timer.start();
    
    SLEPcEigenSolver spSolver(MPI_COMM_SELF, RR);
    spSolver.parameters["solver"] = "krylov-schur";
    spSolver.parameters["spectrum"] = "largest real";
    // spSolver.parameters["problem_type"] = "gen_hermitian";
    spSolver.solve(numEigens);
            
    // get eigenvectors
    double lmd, v;
    PETScVector rx, vx;
    double sumLmd = 0;
    for(int ei = 0; ei < numEigens; ei++){
        spSolver.get_eigenpair(lmd, v, rx, vx, ei);
        
        std::string fn = fileNameOut + "basis-" + std::to_string(ei);
        remove(fn.data());
        std::ofstream outFile(fn, std::ios_base::app);
        std::string outbuffer = std::to_string(dN) + "\n";
        
        evec.vector().get()->zero();
        double scale = std::sqrt(lmd);// * std::sqrt(0.75*dN);
        for(int i = 0; i < dN; i++){
            double val = scale * rx.getitem(i);
            evec.vector().get()->setitem(i, val);
            outbuffer += std::to_string(val) + "\n";
        }
        evec.vector().get()->apply("insert");
        // efile << evec;

        outFile << outbuffer;
        outFile.close();

        sumLmd += lmd;
        // info("lambda[%d] = %g (%g, %g) and scale %d", ei, lmd, rx.min(), rx.max(), scale);
        // info("%d %g", ei, lmd);
    } 
    info("Sum lmd = %g, solve Time=%g", sumLmd, timer.stop() );
}

void kle3d(double Lx, double Ly, double Lz, int Nx, int Ny, int Nz, 
int covtype, double lx, double ly, double lz, double sigma2, int numEigens, std::string fileNameOut){
    Timer timer("SOLUTION");
    timer.start();

    double dx = lx/Nx;
    double dy = ly/Ny;
    double dz = lz/Nz;
    info("d %g, %g, %g ", dz, dy, dz);

    Mesh dmesh; 
    build3d(dmesh, Nx, Ny, Nz, 0, Lx, 0, Ly, 0, Lz);
    auto mesh = std::make_shared<Mesh>(dmesh);

    auto Ve = std::make_shared<Pressure3d::FunctionSpace>(mesh);
    Function evec(Ve);
    // File efile(efilename);

    int dN = mesh->num_cells();
    info("generate covariance matrix R[%d, %d]", dN, dN);
    
    Mat CovR;
    MatCreateSeqAIJ(PETSC_COMM_SELF, dN, dN, dN, 0, &CovR);
    for(int i = 0; i < dN; i++){
        Cell v1(*mesh, i);
        double x1 = v1.midpoint().x();
        double y1 = v1.midpoint().y();
        double z1 = v1.midpoint().z();
        for(int j = 0; j < dN; j++){
            Cell v2(*mesh, j);
            double x2 = v2.midpoint().x();
            double y2 = v2.midpoint().y();
            double z2 = v2.midpoint().z();
            double val = covarianceR3d(covtype, x1, y1, z1, x2, y2, z2, lx, ly, lz, sigma2)*dx*dy*dz; 
            MatSetValues(CovR, 1, &i, 1, &j ,&val, INSERT_VALUES);
        }
    }
    MatAssemblyBegin(CovR, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(CovR, MAT_FINAL_ASSEMBLY);
    
    auto RR = std::make_shared<PETScMatrix>(CovR);
    if(RR->size(0) < numEigens)
        numEigens = RR->size(0);
    info("RR with %d Time=%g", numEigens, timer.stop() );
    timer.start();
    
    SLEPcEigenSolver spSolver(MPI_COMM_SELF, RR);
    spSolver.parameters["solver"] = "krylov-schur";
    spSolver.parameters["spectrum"] = "largest real";
    spSolver.parameters["problem_type"] = "gen_hermitian";
    spSolver.solve(numEigens);

    int ccount = spSolver.get_number_converged();
    numEigens = (numEigens > ccount)?ccount:numEigens;
    info("ecount %d ", numEigens);

    // get eigenvectors
    double lmd, v;
    PETScVector rx, vx;
    double sumLmd = 0;
    for(int ei = 0; ei < numEigens; ei++){
        spSolver.get_eigenpair(lmd, v, rx, vx, ei);
        
        std::string fn = fileNameOut + "basis-" + std::to_string(ei);
        remove(fn.data());
        std::ofstream outFile(fn, std::ios_base::app);
        std:: string outbuffer = std::to_string(dN) + "\n";   
        
        evec.vector().get()->zero();
        double scale = std::sqrt(lmd);// * std::sqrt(0.75*dN);
        for(int i = 0; i < dN; i++){
            double val = scale * rx.getitem(i);
            evec.vector().get()->setitem(i, val);
            outbuffer += std::to_string(val) + "\n";
        }
        evec.vector().get()->apply("insert");
        // efile << evec;

        outFile << outbuffer;
        outFile.close();

        sumLmd += lmd;
        // info("lambda[%d] = %g (%g, %g) and scale %d", ei, lmd, rx.min(), rx.max(), scale);
        // info("%d %g", ei, lmd);
    }
    info("Sum lmd = %g, solve Time=%g", sumLmd, timer.stop() );
}

#endif
