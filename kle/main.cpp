
#include "kle.h"
#include <fstream>
#include <iostream>
#include <cmath>

using namespace dolfin;


// 2d
// ./solver 100 100 0 200 0.2 0.2 0 2.0 1 ../data-2d/kle/
// 3d
// ./solver 20 20 20 200 0.2 0.2 0.2 2.0 1 ../data-3d/kle/
int main(int argc, char** argv) {
    PetscInitialize(&argc, &argv, (char*)0, "");
    parameters["reorder_dofs_serial"] = false;
    Timer timer("SOLUTION");
    timer.start();
    
    int NNx = atoi(argv[1]);
    int NNy = atoi(argv[2]);
    int NNz = atoi(argv[3]);

    int eNum = std::atoi(argv[4]);//100;//50;

    double corr_lx = std::atof(argv[5]);//0.2 0.8
    double corr_ly = std::atof(argv[6]);//0.2
    double corr_lz = std::atof(argv[7]);//0.2 
    double sigma2 = std::atof(argv[8]);//1.0;//2.0;// 4.0;

    int covtype = std::atoi(argv[9]);//1;// 2;
    std::string outdir = argv[10];

    int dim = (NNz == 0)?2:3;
    double Lx = 1.0, Ly = 1.0, Lz = 1.0;

    // save scaled eigenvectors
    timer.start();
    if(dim == 2)
        kle2d(Lx, Ly, Lz, NNx, NNy, NNz, covtype, corr_lx, corr_ly, sigma2, eNum, outdir);
    if(dim == 3)
        kle3d(Lx, Ly, Lz, NNx, NNy, NNz, covtype, corr_lx, corr_ly, corr_lz, sigma2, eNum, outdir);
    info("init, Time = %g", timer.stop() );

    PetscFinalize();
    return 0;
}
