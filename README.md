# ms-mcmc

In this work, we consider the numerical solution of the poroelasticity problem with stochastic properties. 
We present a Two-stage Markov Chain Monte Carlo method for geomechanical subsidence. 
In this work, we study two techniques of preconditioning: 
(MS) multiscale method for model order reduction and 
(ML) machine learning technique. 
The purpose of preconditioning is the fast sampling, where a new proposal is first testes by a cheap multiscale solver or using fast prediction of the neural network and the full fine grid computations will be conducted only if the proposal passes the first step. 
To construct a reduced order model, we use the Generalized Multiscale Finite Element Method and present construction of the multiscale basis functions for pressure and displacements in stochastic fields. 
In order to construct a machine learning based preconditioning, we generate a dataset using a multiscale solver and use it to train neural networks. 
The Karhunen-Loeve expansion is used to represent the realization of the stochastic field.  Numerical results are presented for two- and three-dimensional model examples. 

Implementation of:

* GMsFEM for solution of the poroelasticity problem with universal basis functions.

* Generation of the random media properties using KLE.

* One-stage MCMC 

* Two-stage MCMC using GMsFEM coarse grid solver 

* Two-stage MCMC using Machine Learning (CNN for prediction of observation data)

Implementation based on the [FEniCS](https://fenicsproject.org) and [Keras](https://keras.io).

Please cite the paper: [Preconditioning Markov Chain Monte Carlo Method for Geomechanical Subsidence using multiscale method and machine learning technique. Maria Vasilyeva, Aleksei Tyrylgin,  Donald L. Brown, Anirban Mondal.](https://arxiv.org/abs/2007.00066).


## Citation

    @article{vasilyeva2020preconditioning,
      title={Preconditioning Markov Chain Monte Carlo Method for Geomechanical Subsidence using multiscale method and machine learning technique},
      author={Maria Vasilyeva and Aleksei Tyrylgin and Donald L. Brown and Anirban Mondal},
      journal={arXiv preprint arXiv:2007.00066},
      year={2020}
    }

## How to use

1. Generate kle basis for random media properties generation (kle basis generations in coded on c++)
	* (2D) ./solver 100 100 0 200 0.2 0.2 0 2.0 1 ../data-2d/kle/
	* (3D) ./solver 20 20 20 200 0.2 0.2 0.2 2.0 1 ../data-3d/kle/

2. Generate k and E using kle basis
	* (2D) python kle-coeff.py -d 2 -k 200 -n 18
	* (3D) python kle-coeff.py -d 3 -k 200 -n 18

3. Solve on fine grid
	* (2D) python solver-fine.py -d 2 -n 7 -g 10000
	* (3D) python solver-fine.py -d 3 -n 7 -g 10000

4. Construct and save gmsfem basis for u and p (./msbasis)
	* (2D) python gmsfem-up.py -d 2 -n 7 -t 2
	* (3D) python gmsfem-up.py -d 3 -n 7 -t 2

5. Solve on coarse grid
	* (2D) python solver-ms.py -d 2 -n 7
	* (3D) python solver-ms.py -d 3 -n 7

6. Testing for random media (t = 1 test ms with different number of basis functions)
	* (2D) python solver-ms-r.py -d 2 -n 7 -g 10000 -t 1 -m 0 -o ./results
	* (3D) python solver-ms-r.py -d 2 -n 7 -g 10000 -t 1 -m 0 -o ./results

7. Testing for random media (t = 2 test ms for random fields realizations)
	* (2D) python solver-ms-r.py -d 2 -n 7 -g 10000 -t 2 -m 50 -o ./results
	* (3D) python solver-ms-r.py -d 2 -n 7 -g 10000 -t 2 -m 50 -o ./results

8. MCMC solver (t = 4 one-stage mcmc)
	* (2D) python solver-mcmc.py -d 2 -n 7 -g 10000 -t 4 -m 500 -beta 2 -sf 0.02 -dlt 0.5  -o ./results
	* (3D) python solver-mcmc.py -d 2 -n 7 -g 10000 -t 4 -m 500 -beta 2 -sf 0.02 -dlt 0.5  -o ./results

9. MCMC solver (t = 3 two-stage mcmc using multiscale solver)
	* (2D) python solver-mcmc.py -d 2 -n 7 -g 10000 -t 3 -m 500 -beta 2 -sf 0.02 -dlt 0.5  -o ./results
	* (3D) python solver-mcmc.py -d 2 -n 7 -g 10000 -t 3 -m 500 -beta 2 -sf 0.02 -dlt 0.5  -o ./results

10. MCMC solver (t = 4 two-stage mcmc using ML prediction, before use it train a CNN)
	* (2D) python solver-mcmc.py -d 2 -n 7 -g 10000 -t 5 -m 500 -beta 2 -sf 0.02 -dlt 0.5  -o ./results
	* (3D) python solver-mcmc.py -d 2 -n 7 -g 10000 -t 5 -m 500 -beta 2 -sf 0.02 -dlt 0.5  -o ./results
