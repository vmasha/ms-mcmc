import numpy as np
from dolfin import *
import math, argparse, sys
parameters["reorder_dofs_serial"] = False;
parameters["allow_extrapolation"] = True;
timer = Timer()


# python gmsfem-up.py -d 2 -n 1 -t 2
parser = argparse.ArgumentParser()
parser.add_argument('-d', type=int, required=True,  help="dim")
parser.add_argument('-n', type=int, required=True,  help="realizaton number (k and E)")
parser.add_argument('-t', type=int, required=True,  help="coeff type")
args = parser.parse_args()

dim = args.d
lind = args.n
ktype = args.t
print("gmsfem basis for %d realization (%dd problem)" % (lind, dim) )

ecountU = 16
ecountP = 16

if(dim == 2):
    kledir = '../data-2d/kle/'
    outdirP = '../data-2d/ms-p/'
    outdirU = '../data-2d/ms-u/'
    Nx, Ny, Nz = 100, 100, 1
    Nc = 10
if(dim == 3):
    kledir = '../data-3d/kle/'
    outdirP = '../data-3d/ms-p/'
    outdirU = '../data-3d/ms-u/'
    Nx, Ny, Nz = 20, 20, 20
    Nc = 5

nu = 0.3

cH = 1.0/Nc 
lNx, lNy, lNz = int(Nx/Nc), int(Ny/Nc), int(Nz/Nc)

NN = (Nc+1)**dim

# =========================
# ===== LOAD K and E ======
# =========================
if ktype == 1:
    my_dataK = np.loadtxt(kledir+'k-'+str(lind), dtype=float)
    my_dataE = np.loadtxt(kledir+'E-'+str(lind), dtype=float)
    print("load k and E for %d" % lind)

if ktype == 2:
    NQc = Nx*Ny
    if dim == 3:
        NQc = Nx*Ny*Nz
    # find average
    my_dataK = np.zeros(NQc)
    my_dataE = np.zeros(NQc)
    Nri = lind
    for ri in range(lind):
        my_dataKi = np.loadtxt(kledir+'k-'+str(ri), dtype=float)
        my_dataEi = np.loadtxt(kledir+'E-'+str(ri), dtype=float)
        for i in range(NQc):
            my_dataK[i] += my_dataKi[i]
            my_dataE[i] += my_dataEi[i]
        print("load k and E for %d" % ri)
    for i in range(NQc):
        my_dataK[i] /= Nri
        my_dataE[i] /= Nri
# reshape
if(dim == 2):
    arrK = np.reshape(my_dataK, (Nx, Ny))
    arrE = np.reshape(my_dataE, (Nx, Ny))
if(dim == 3):
    arrK = np.reshape(my_dataK, (Nx, Ny, Nz))
    arrE = np.reshape(my_dataE, (Nx, Ny, Nz))
print('K:', arrK.min(), arrK.max())
print('E:', arrE.min(), arrE.max())
print(arrK.shape)


# ====================
# ===== CLASSES ======
# ====================
class HeterExpressionLoc(Expression):
    def setArr(self, arr, inci, incj, inck, Nx, Ny, Nz):
        self.arr = arr
        self.inci = inci
        self.incj = incj
        self.inck = inck
        self.Nx = Nx
        self.Ny = Ny
        self.Nz = Nz
    def value_shape(self):
        return ()    
    def eval_cell(self, value, x, ufc_cell):
        if(dim == 2):
            ci = ufc_cell.index/2
            locj, loci = divmod(ci, self.Nx)
            gi = self.inci + loci
            gj = self.incj + locj
            value[0] = self.arr[int(gi), int(gj)]
        if(dim == 3):
            ci = ufc_cell.index/6
            loclj, loci = divmod(ci, self.Nx)
            locl, locj = divmod(loclj, self.Ny)
            gi = self.inci + loci
            gj = self.incj + locj
            gl = self.inck + locl
            value[0] = self.arr[int(gi), int(gj), int(gl)]

class LinBasis(Expression):
    def setind(self, ind, p1, p2):
        self.ind = ind
        self.hx = p2.x() - p1.x()
        self.hy = p2.y() - p1.y()
        self.p1 = p1; self.p2 = p2
        if dim == 2:
            self.vx = [p1.x(), p2.x(), p1.x(), p2.x()]
            self.vy = [p1.y(), p1.y(), p2.y(), p2.y()]
        if dim == 3:
            self.hz = p2.z() - p1.z()
            self.vx = [p1.x(), p2.x(), p1.x(), p2.x(),  p1.x(), p2.x(), p1.x(), p2.x()]
            self.vy = [p1.y(), p1.y(), p2.y(), p2.y(),  p1.y(), p1.y(), p2.y(), p2.y()]        
            self.vz = [p1.z(), p1.z(), p1.z(), p1.z(),  p2.z(), p2.z(), p2.z(), p2.z()]  
    def value_shape(self):
        return ()
    def eval(self, value, x):
        #
        if self.ind == 0 or self.ind == 4:
            val =  (x[0] - self.vx[1])/self.hx * (x[1] - self.vy[3])/self.hy
            if dim == 3 and self.ind == 0:
                val = -(x[0] - self.vx[1])/self.hx * (x[1] - self.vy[3])/self.hy*(x[2] - self.vz[4])/self.hz
            if dim == 3 and self.ind == 4:
                val =  (x[0] - self.vx[1])/self.hx * (x[1] - self.vy[3])/self.hy*(x[2] - self.vz[0])/self.hz
        #        
        if self.ind == 1 or self.ind == 5:
            val = -(x[0] - self.vx[0])/self.hx * (x[1] - self.vy[2])/self.hy
            if dim == 3 and self.ind == 1:
                val =  (x[0] - self.vx[0])/self.hx * (x[1] - self.vy[2])/self.hy*(x[2] - self.vz[5])/self.hz
            if dim == 3 and self.ind == 5:
                val = -(x[0] - self.vx[0])/self.hx * (x[1] - self.vy[2])/self.hy*(x[2] - self.vz[1])/self.hz
        #   
        if self.ind == 2 or self.ind == 6:
            val = -(x[0] - self.vx[3])/self.hx * (x[1] - self.vy[1])/self.hy;
            if dim == 3 and self.ind == 2:
                val =  (x[0] - self.vx[3])/self.hx * (x[1] - self.vy[1])/self.hy*(x[2] - self.vz[6])/self.hz
            if dim == 3 and self.ind == 6:
                val = -(x[0] - self.vx[3])/self.hx * (x[1] - self.vy[1])/self.hy*(x[2] - self.vz[2])/self.hz
        #   
        if self.ind == 3 or self.ind == 7:
            val =  (x[0] - self.vx[2])/self.hx * (x[1] - self.vy[0])/self.hy
            if dim == 3 and self.ind == 3:
                val = -(x[0] - self.vx[2])/self.hx * (x[1] - self.vy[0])/self.hy*(x[2] - self.vz[7])/self.hz
            if dim == 3 and self.ind == 7:
                val =  (x[0] - self.vx[2])/self.hx * (x[1] - self.vy[0])/self.hy*(x[2] - self.vz[3])/self.hz
        #        
        if (x[0] <= self.p2.x() and x[0] >= self.p1.x() and x[1] <= self.p2.y() and x[1] >= self.p1.y())\
        and (dim == 2 or (dim == 3 and (x[2] <= self.p2.z() and x[2] >= self.p1.z()) ) ):
            value[0] = val
        else:
            value[0] = 0.0       

# gmsfem
def getIndex(i, j, k, Nx, Ny):
    if dim == 3:
        return (k*Ny + j)*Nx + i
    else:
        return j*Nx + i
    
# ========================
# ===== GLOBAL MESH ======
# ========================
if dim == 2:
    p1 = Point(0.0, 0.0)
    p2 = Point(1.0, 1.0)
    fmesh = RectangleMesh(p1, p2, Nx, Ny)
if dim == 3:
    p1 = Point(0.0, 0.0, 0.0)
    p2 = Point(1.0, 1.0, 1.0)
    fmesh = BoxMesh(p1, p2, Nx, Ny, Nz)  
    
GVP = FunctionSpace(fmesh, "CG", 1)
GVU = VectorFunctionSpace(fmesh, "CG", 1)
gp = Function(GVP); gp.rename('u', '0')
gu = Function(GVU); gu.rename('u', '0')
Nv = fmesh.num_vertices()

# fileK = File('./results/k.pvd')
# fileE = File('./results/E.pvd')
# fileU = File('./results/u.pvd')
# fileP = File('./results/p.pvd')

# ==================
# ===== SOLVE ======
# ==================
for wi in range(NN):
    # mesh
    pous = []
    if(dim == 2):
        coj, coi = divmod(wi, (Nc+1)); cok = 0
        
        i0, j0, k0 = coi, coj, 0
        i1, j1, k1 = coi, coj, 0
        if coi != 0:  i0 = (coi-1);
        if coj != 0:  j0 = (coj-1);
        if coi != Nc: i1 = (coi+1);
        if coj != Nc: j1 = (coj+1);
        for ii in range(i0, i1):
            for jj in range(j0, j1):
                # cells of w
                if getIndex(ii,   jj,   0, Nc+1, Nc+1) == wi: cind = 0;
                if getIndex(ii+1, jj,   0, Nc+1, Nc+1) == wi: cind = 1;
                if getIndex(ii,   jj+1, 0, Nc+1, Nc+1) == wi: cind = 2;
                if getIndex(ii+1, jj+1, 0, Nc+1, Nc+1) == wi: cind = 3;
                pou = LinBasis(degree=0)
                pou.setind(cind, Point(ii*cH, jj*cH), Point((ii+1)*cH, (jj+1)*cH))
                pous.append(pou)
        
        p1 = Point( i0*cH, j0*cH )
        p2 = Point( i1*cH, j1*cH )
        locNx = lNx*(i1-i0)
        locNy = lNy*(j1-j0)
        locNz = 0
        locmesh = RectangleMesh(p1, p2, locNx, locNy)
    if(dim == 3):
        cokj, coi = divmod(wi, (Nc+1))
        cok, coj = divmod(cokj, (Nc+1))
        
        i0, j0, k0 = coi, coj, cok
        i1, j1, k1 = coi, coj, cok
        if coi != 0:  i0 = (coi-1);
        if coj != 0:  j0 = (coj-1);
        if cok != 0:  k0 = (cok-1);            
        if coi != Nc: i1 = (coi+1);
        if coj != Nc: j1 = (coj+1);
        if cok != Nc: k1 = (cok+1);
        for ii in range(i0, i1):
            for jj in range(j0, j1):
                for kk in range(k0, k1):
                    # cells of w
                    if getIndex(ii,   jj,   kk, Nc+1, Nc+1) == wi: cind = 0;
                    if getIndex(ii+1, jj,   kk, Nc+1, Nc+1) == wi: cind = 1;
                    if getIndex(ii,   jj+1, kk, Nc+1, Nc+1) == wi: cind = 2;
                    if getIndex(ii+1, jj+1, kk, Nc+1, Nc+1) == wi: cind = 3;
                    if getIndex(ii,   jj,   kk+1, Nc+1, Nc+1) == wi: cind = 4;
                    if getIndex(ii+1, jj,   kk+1, Nc+1, Nc+1) == wi: cind = 5;
                    if getIndex(ii,   jj+1, kk+1, Nc+1, Nc+1) == wi: cind = 6;
                    if getIndex(ii+1, jj+1, kk+1, Nc+1, Nc+1) == wi: cind = 7;
                    pou = LinBasis(degree=0)
                    pou.setind(cind, Point(ii*cH, jj*cH, kk*cH), Point((ii+1)*cH, (jj+1)*cH, (kk+1)*cH))
                    pous.append(pou)
        
        p1 = Point( i0*cH, j0*cH, k0*cH )
        p2 = Point( i1*cH, j1*cH, k1*cH )
        locNx = lNx*(i1-i0)
        locNy = lNy*(j1-j0)
        locNz = lNz*(k1-k0)
        locmesh = BoxMesh(p1, p2, locNx, locNy, locNz)  
    locmesh.init()
    volK = 0.0
    for i in range(locmesh.num_cells()):
        cell = Cell(locmesh, i)
        volK += cell.volume()
    if wi%10 == 0:
        print("local : %d = [%d, %d, %d] for (%d, %d, %d) - (%d, %d, %d) with %d cells (volK %g)" %
              (wi, coi, coj, cok, i0, j0, k0, i1, j1, k1, len(pous), volK) )

    # local coeff k
    kLoc = HeterExpressionLoc(degree = 0)
    kLoc.setArr(arrK, i0*lNx, j0*lNy, k0*lNz, locNx, locNy, locNz)
    # local coeff E
    ELoc = HeterExpressionLoc(degree = 0)
    ELoc.setArr(arrE, i0*lNx, j0*lNy, k0*lNz, locNx, locNy, locNz)
    # save
#     V0 = FunctionSpace(locmesh, "DG", 0)
#     u0 = Function(V0); u0.rename('u', '0')
#     u0.interpolate(ELoc); fileE << u0
#     u0.interpolate(kLoc); fileK << u0
     
    # pou
    VPou = FunctionSpace(locmesh, "CG", 1)
    wpou = Function(VPou); wpou.rename('u', '0')
    sol = Function(VPou); sol.rename('u', '0')
    Nwi = wpou.vector().size()
    arrpou = np.array(wpou.vector())
    for pou in pous:
        sol.interpolate(pou)
        arrsol = np.array(sol.vector())
        for i in range(Nwi):
            if arrpou[i] < 1.0e-5:
                arrpou[i] += arrsol[i]
    wpou.vector()[:] = arrpou
#     fileP << wpou
    
    # ----- p -----
    # dof
    lgmapP = []
    incri = i0*(lNx)
    incrj = j0*(lNy)
    incrk = k0*(lNz)
    for vi in range(Nwi):
        if dim == 2:
            lj, li = divmod(vi, locNx+1)
            lk = 0
            gi = incri + li
            gj = incrj + lj
            gk = 0
        if dim == 3:
            lkj, li = divmod(vi, locNx+1)
            lk, lj = divmod(lkj, locNy+1)
            gi = incri + li
            gj = incrj + lj
            gk = incrk + lk
        gvi = getIndex(gi, gj, gk, Nx+1, Ny+1)
        lgmapP.append(gvi)
    np.savetxt(outdirP + 'dof/w'+str(wi), lgmapP, fmt='%d')
#     arrgu = np.zeros(Nv)
#     for vi in range(Nwi):
#         gvi = lgmapP[vi]
#         arrgu[gvi] = 1
#     gp.vector()[:] = arrgu
#     fileP << gp

    # space
    VP = FunctionSpace(locmesh, "CG", 1)
    p = TrialFunction(VP)
    z = TestFunction(VP)
    ap = inner(kLoc*grad(p), grad(z))*dx
    sp = kLoc*p*z*dx
    AP = PETScMatrix() 
    SP = PETScMatrix() 
    assemble(ap, tensor=AP)
    assemble(sp, tensor=SP)
    # solver
    eigensolver = SLEPcEigenSolver(AP, SP)
    # eigensolver.parameters['problem_type'] = 'gen_hermitian'
    # eigensolver.parameters['spectral_transform'] = 'shift-and-invert'
    # eigensolver.parameters['spectral_shift'] = 1.0e-3
    eigensolver.parameters["spectrum"] = "smallest real"#magnitude
    # solve
    eigensolver.solve(ecountP*2)
    print('P[%d] %d' % (wi, eigensolver.get_number_converged()))
    # save
    for i in range(ecountP):
        r, c, rx, cx = eigensolver.get_eigenpair(i)
        arrsol = np.array(rx)
        if i == 0:
            for di in range(Nwi):
                arrsol[di] = 1.0
#         sol.vector()[:] = arrsol
#         fileP << sol
#         print('P [%d]: eigenvalue: %g and ev (%g, %g)' % (wi, r, rx.min(), rx.max()))
        for vi in range(Nwi):
            arrsol[vi] *= arrpou[vi]
        np.savetxt(outdirP + 'eigen/w'+str(wi)+'-e'+str(i), arrsol, fmt='%g')

    # ----- u -----
    # dof
    lgmapU = []
    incri = i0*(lNx)
    incrj = j0*(lNy)
    incrk = k0*(lNz)
    for vi in range(Nwi):
        if dim == 2:
            lj, li = divmod(vi, locNx+1)
            lk = 0
            gi = incri + li
            gj = incrj + lj
            gk = 0
        if dim == 3:
            lkj, li = divmod(vi, locNx+1)
            lk, lj = divmod(lkj, locNy+1)
            gi = incri + li
            gj = incrj + lj
            gk = incrk + lk
        gvi = getIndex(gi, gj, gk, Nx+1, Ny+1)
        lgmapU.append(gvi)
    for di in range(Nwi, Nwi*dim):
        if di < 2*Nwi:
            lgmapU.append(lgmapU[di-Nwi] + Nv)
        else:
            lgmapU.append(lgmapU[di-2*Nwi] + 2*Nv)
    np.savetxt(outdirU + 'dof/w'+str(wi), lgmapU, fmt='%d')
#     arrgu = np.zeros(Nv*dim)
#     for vi in range(Nwi*dim):
#         gvi = lgmapU[vi]
#         arrgu[gvi] = 1
#     gu.vector()[:] = arrgu
#     fileU << gu
    
    # space
    VU = VectorFunctionSpace(locmesh, "CG", 1)
    solu = Function(VU); solu.rename('u', '0')
    u = TrialFunction(VU)
    v = TestFunction(VU)
    lmdLoc = nu*ELoc/(1+nu)/(1-2*nu)
    muLoc  = ELoc/2/(1+nu)
    def epsilon(u):
        return 0.5*(grad(u) + grad(u).T)
    def sigma(u):
        return 2*muLoc*epsilon(u) + lmdLoc*tr(epsilon(u))*Identity(dim)
    au = inner(sigma(u), epsilon(v))*dx
    su = (lmdLoc + 2*muLoc)*inner(u, v)*dx
    AU = PETScMatrix() 
    SU = PETScMatrix() 
    assemble(au, tensor=AU)
    assemble(su, tensor=SU)
    # solver
    eigensolver2 = SLEPcEigenSolver(AU, SU)
    # eigensolver2.parameters['problem_type'] = 'gen_hermitian'
    # eigensolver2.parameters['spectral_transform'] = 'shift-and-invert'
    # eigensolver2.parameters['spectral_shift'] = 1.0e-5
    eigensolver2.parameters["spectrum"] = "smallest real"
    # solve
    eigensolver2.solve(ecountU*2)
    print('U[%d] %d' % (wi, eigensolver2.get_number_converged()))
    # save
    for i in range(ecountU):
        r, c, rx, cx = eigensolver2.get_eigenpair(i)
        arrsol = np.array(rx)
        if i < dim:
            for di in range(Nwi):
                for dd in range(dim):
                    arrsol[di+dd*Nwi] = 0.0
                if i == 0:
                    arrsol[di] = 1.0
                if i == 1:
                    arrsol[di+Nwi] = 1.0
                if dim == 3 and i == 2:
                    arrsol[di+2*Nwi] = 1.0
#         solu.vector()[:] = arrsol
#         fileU << solu
#         print('U [%d]: eigenvalue: %g and ev (%g, %g)' % (wi, r, rx.min(), rx.max()))
        for di in range(Nwi*dim):
            pi = di%Nwi
            arrsol[di] *= arrpou[pi]
        np.savetxt(outdirU + 'eigen/w'+str(wi)+'-e'+str(i), arrsol, fmt='%g')

print("DONE")    

