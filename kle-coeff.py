import numpy as np
import math, argparse
import os

# python3 kle-coeff.py -d 2 -k 200 -n 18
# python3 kle-coeff.py -d 3 -k 200 -n 18
parser = argparse.ArgumentParser()
parser.add_argument('-d', type=int, required=True,  help="dim")
parser.add_argument('-k', type=int, required=True,  help="number of kle basis")
parser.add_argument('-n', type=int, required=True,  help="number of realizaton")
args = parser.parse_args()

dim = args.d
klecount = args.k
Nr = args.n
print("KLE based k and E generator dim = %d, kle basis count = %d, generate Nr = %d" % (dim, klecount, Nr))

minphi = 0.05
maxphi = 0.2
print("rescale: phi in (%g, %g)" % (minphi, maxphi))

if(dim == 2):
    kledir = './data-2d/kle/'
    Nx, Ny, Nz = 100, 100, 1
if(dim == 3):
    kledir = './data-3d/kle/'
    Nx, Ny, Nz = 20, 20, 20
    
N = Nx*Ny*Nz
print("Mesh %d, save to %s" % (N, kledir))

# ----------------------
# --- load kle basis ---
# ----------------------
klebases = []
for ki in range(klecount):
    infile0 = kledir + 'basis-' + str(ki)
    with open(infile0) as f:
        lines_list = f.readlines()
        my_data = [ float(val) for val in lines_list[1::]]
        klebases.append(np.copy(my_data))
print(len(klebases), klecount) 

# ------------------------
# --- generate k and E ---
# ------------------------ 
for ri in range(Nr):
    print(ri)
    # generate random field
    my_data0 = np.zeros(N)
    for ki in range(klecount):
        rr = np.random.normal(0.0, 1.0)
        for i in range(N):
            my_data0[i] += rr * klebases[ki][i]
    
    # rescale and find porosity
    my_data0[:] = (my_data0[:] - my_data0.min())/(my_data0.max() - my_data0.min())
    my_data0[:] = my_data0[:]*(maxphi-minphi) + minphi
    
    # find k and E
    my_dataK = np.zeros(N)
    my_dataE = np.zeros(N)
    for i in range(N):
        val = my_data0[i]
        my_dataK[i] = math.exp(40*val)
        my_dataE[i] = 0.1*math.pow((1.0 - val)/val, 1.5)
    print('phi:', my_data0.min(), my_data0.max())
    print('K:',   my_dataK.min(), my_dataK.max())
    print('E:',   my_dataE.min(), my_dataE.max())

    # save
    fileM = kledir + 'm-' + str(ri)
    fileK = kledir + 'k-' + str(ri)
    fileE = kledir + 'E-' + str(ri)
    np.savetxt(fileM, my_data0, fmt='%g')
    np.savetxt(fileK, my_dataK, fmt='%g')
    np.savetxt(fileE, my_dataE, fmt='%g')
    
print("DONE")



