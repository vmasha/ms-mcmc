import math, argparse, sys
from random import random, seed
import numpy as np
from dolfin import *
sys.path.append('./')
from PUProblem import *
petsc4py.init(sys.argv)
parameters["reorder_dofs_serial"] = False;
parameters["allow_extrapolation"] = True;
timer = Timer()
timerGl = Timer()
seed(961)

# python solver-mcmc.py -d 2 -n 7 -g 10000 -t 2 -m 100 -o ./results-t2/
# python solver-mcmc.py -d 2 -n 7 -g 10000 -t 3 -m 1000 -beta 2 -sf 0.02 -dlt 0.5 -o ./results-t3-2s-d0c5-sf0c02-b2-M1/
# python solver-mcmc.py -d 2 -n 7 -g 10000 -t 33 -m 1000 -beta 2 -sf 0.02 -dlt 0.5 -o ./results-t3-2s-d0c5-sf0c02-b2-ML/
parser = argparse.ArgumentParser()
parser.add_argument('-d', type=int, required=True,  help="dim")
parser.add_argument('-n', type=int, required=True,  help="realizaton number (k and E)")
parser.add_argument('-g', type=int, required=True,  help="gamma for Robyn bc")
parser.add_argument('-t', type=int, required=True,  help="type: 1 - check ms for exact (lind = n), 2 - check F_obs, 3 - two-stage mcmc, 4 - one-stage mcmc")
parser.add_argument('-m', type=int, required=True,  help="number of random fields")
parser.add_argument('-o', dest='outdir',  help="output folder")

parser.add_argument('-beta', type=int, default=1, help="beta for mcmc")
parser.add_argument('-sf', type=float, default=0.02, help="sigmaf for mcmc")
parser.add_argument('-dlt', type=float, default=0.5, help="delta for mcmc")

args = parser.parse_args()

dim = args.d
lind = args.n
gamma = args.g

mtype = args.t
mnum = args.m

outdir = args.outdir
beta = args.beta
sf = args.sf
delta = args.dlt


print("MCMC poroelastic on coarse grid, for %d realization (%dd problem ) out=%s" % (lind, dim, outdir) )

if(dim == 2):
    indir = './data-2d/'
    Nx, Ny, Nz = 100, 100, 1
    Ncx, Ncy, Ncz = 10, 10, 1
    tmax = 0.001
if(dim == 3):
    indir = './data-3d/'
    Nx, Ny, Nz = 20, 20, 20
    Ncx, Ncy, Ncz = 5, 5, 5
    tmax = 0.001

tcount = 20 
dt = tmax/tcount

alpha = 0.1
c = 1.0
nu = 0.3
g1 = 1.0
print("Tmax = %g with %d time steps" % (tmax, tcount))

Ncv = (Ncx+1)*(Ncy+1)
if dim ==3:
    Ncv = (Ncx+1)*(Ncy+1)*(Ncz+1)
print(Ncv)

Nfc = Nx*Ny
if dim == 3:
    Nfc = Nx*Ny*Nz
print(Nfc)

# number of basis
bcc = []
if mtype == 3:
    bcc = [2]

# ===========================
# ===== LOAD EXACT k,E ======
# ===========================
my_dataK = np.loadtxt(indir+'kle/k-'+str(lind), dtype=float)
my_dataE = np.loadtxt(indir+'kle/E-'+str(lind), dtype=float)
if(dim == 2):
    arrK0 = np.reshape(my_dataK, (Nx, Ny))
    arrE0 = np.reshape(my_dataE, (Nx, Ny))
if(dim == 3):
    arrK0 = np.reshape(my_dataK, (Nx, Ny, Nz))
    arrE0 = np.reshape(my_dataE, (Nx, Ny, Nz)) 
print('K:', arrK0.min(), arrK0.max())
print('E:', arrE0.min(), arrE0.max())
print(arrK0.shape)  

# =====================
# ===== LOAD KLE ======
# =====================
minphi = 0.05
maxphi = 0.2

klecount = 200
klebases = np.zeros( (klecount, Nfc) )
for ki in range(klecount):
    infile0 = indir + 'kle/basis-' + str(ki)
    with open(infile0) as f:
        lines_list = f.readlines()
        j = 0
        for val in lines_list[1::]:
            klebases[ki, j] = float(val)
            j += 1
print(len(klebases), klecount)

def generateKLE(rcoefs):
    my_data0 = np.matmul(rcoefs, klebases)
    # my_data0 = np.zeros(Nfc)
    # for ki in range(klecount):
    #     for i in range(Nfc):
    #         my_data0[i] += rcoefs[ki] * klebases[ki][i]
    # rescale and find porosity
    my_data0[:] = (my_data0[:] - my_data0.min())/(my_data0.max() - my_data0.min())
    my_data0[:] = my_data0[:]*(maxphi - minphi) + minphi
    print('phi:', my_data0.min(), my_data0.max())
    return my_data0

# generate k and E
def generateK(my_data0):
    my_dataK = np.zeros(Nfc)
    for i in range(Nfc):
        val = my_data0[i]
        my_dataK[i] = math.exp(40*val)
    print('K:',   my_dataK.min(), my_dataK.max())
    if dim == 2:
        arrK = np.reshape(my_dataK, (Nx, Ny))
    if dim == 3:
        arrK = np.reshape(my_dataK, (Nx, Ny, Nz))
    return arrK

def generateE(my_data0):        
    my_dataE = np.zeros(Nfc)
    for i in range(Nfc):
        val = my_data0[i]
        my_dataE[i] = 0.1*math.pow((1.0 - val)/val, 1.5)
    print('E:',   my_dataE.min(), my_dataE.max())
    if dim == 2:
        arrE = np.reshape(my_dataE, (Nx, Ny))
    if dim == 3:
        arrE = np.reshape(my_dataE, (Nx, Ny, Nz))
    return arrE

def getVIndex(i, j, k, Nx, Ny):
    if dim == 3:
        return (k*(Ny+1) + j)*(Nx+1) + i
    else:
        return j*(Nx+1) + i
    
# observation data
def findF(sol):
    arrsol = np.array(sol.vector())
    if dim == 2:
        Nv1 = (Nx+1)*(Ny+1)
        Nv2 = Nv1*2
        obsdata = np.zeros( (Nx+1)*dim )
        icount = 0
        for i in range(Nx+1):
            vi = getVIndex(i, Ny, 0, Nx, Ny) + Nv1
            obsdata[icount] = arrsol[vi]; icount += 1
            vi = getVIndex(i, Ny, 0, Nx, Ny) + Nv2
            obsdata[icount] = arrsol[vi]; icount += 1
    if dim == 3:
        Nv1 = (Nx+1)*(Ny+1)*(Nz+1)
        Nv2 = Nv1*2
        Nv3 = Nv1*3
        obsdata = np.zeros( (Nx+1)*(Nz+1)*dim )
        icount = 0
        for i in range(Nx+1):
            for j in range(Nz+1):
                vi = getVIndex(i, Ny, j, Nx, Ny) + Nv1
                obsdata[icount] = arrsol[vi]; icount += 1
                vi = getVIndex(i, Ny, j, Nx, Ny) + Nv2
                obsdata[icount] = arrsol[vi]; icount += 1
                vi = getVIndex(i, Ny, j, Nx, Ny) + Nv3
                obsdata[icount] = arrsol[vi]; icount += 1
    return obsdata

def calcerror(arrMs, arrEx, Nvert):
    errp = 0.0; sump = 0.0
    for i in range(Nvert):
        errp += math.pow(arrMs[i] - arrEx[i], 2)
        sump += math.pow(arrEx[i], 2)
    erru = 0.0; sumu = 0.0
    for i in range(Nvert, Nvert*(1+dim)):
        erru += math.pow(arrMs[i] - arrEx[i], 2)
        sumu += math.pow(arrEx[i], 2)
    print('---> ERROR_p = %g, ERROR_u = %g' % (math.sqrt(errp/sump)*100, math.sqrt(erru/sumu)*100) )
    

def solveProblem(isms, problemF, problemMs, k, E, nu, c, dt, alpha):
    timer.start()
    if isms:
        problemMs.projectRART(problemF.A)
    # initial
    if(dim == 2):
        problemF.sol.interpolate( Constant((0.0, 0.0, 0.0)) )
        problemMs.sol.interpolate( Constant((0.0, 0.0, 0.0)) )
    if(dim == 3):
        problemF.sol.interpolate( Constant((0.0, 0.0, 0.0, 0.0)) )
        problemMs.sol.interpolate( Constant((0.0, 0.0, 0.0, 0.0)) )
    # solve
    t = 0; tcounter = 0
    while t < tmax:
        if isms:
            ums0 = problemMs.sol.copy(deepcopy=True)
            problemF.form2rhs(c, dt, ums0, alpha)
            problemMs.projectRb(problemF.b)
            problemMs.solveMs()
            cursol = problemMs.sol
        else: # fine
            un = problemF.sol.copy(deepcopy=True)
            problemF.form2rhs(c, dt, un, alpha)
            problemF.solve()
            cursol = problemF.sol 
        # print('Time[%d] = %f: u(%g, %g)' % (tcounter, t, cursol.vector().min(), cursol.vector().max()))
        t += dt
        tcounter += 1
    print('solution time = %g sec, u(%g, %g)' % (timer.stop(), cursol.vector().min(), cursol.vector().max()) )
    return cursol.copy(deepcopy=True)

bind = []

# =================
# ===== MESH ======
# =================
if dim == 2:
    fimesh = UnitSquareMesh(Nx, Ny)
if dim == 3:
    fimesh = UnitCubeMesh(Nx, Ny, Nz)
Nvert = fimesh.num_vertices()
print('mesh')

V0 = FunctionSpace(fimesh, "DG", 0)
u0 = Function(V0); u0.rename('u', '0')

# =====================
# ===== PROBLEMS ======
# =====================
problemF = PUProblem(dim, fimesh, g1, gamma)
allMsProblems = []
for bi in bcc:
    bcountp = 1 + bi
    bcountu = dim + bi
    problemMs = MsProblem(problemF.V)
    problemMs.loadR(indir, Ncv, bcountp, bcountu, bind, 0, 0, Nvert)
    allMsProblems.append(problemMs)

# ==================
# ===== EXACT ======
# ==================
E = HeterExpression(degree = 0)
E.setArr(dim, Nx, Ny, Nz, arrE0)
k = HeterExpression(degree = 0)
k.setArr(dim, Nx, Ny, Nz, arrK0)

timerGl.start()
problemF.form2mat(c, dt, k, E, nu, alpha)
print("init mat Time %g sec" % timerGl.stop())
solf = solveProblem(False, problemF, problemF, k, E, nu, c, dt, alpha); solf.rename('u', '0') 
arrEx = np.array(solf.vector())
Fobs = findF(solf)
print(len(Fobs))
# print(Fobs)

filep = File(outdir + 'p.pvd')
fileu = File(outdir + 'u.pvd')
fileK = File(outdir + 'k.pvd')
fileE = File(outdir + 'E.pvd')


# ===========================
# ===== TWO-STAGE MCMC ======
# ===========================
if mtype == 3:
    # params 
    sf2 = math.pow(sf, 2)
    sc2 = sf2 * beta
    problemMs = allMsProblems[0]

    # accepted
    fileKA = File(outdir + "kA.pvd")
    fileEA = File(outdir + "EA.pvd")
    filepA = File(outdir + "pA.pvd")
    fileuA = File(outdir + "uA.pvd")

    # save exact
    u0.interpolate(k); fileK << u0
    u0.interpolate(E); fileE << u0
    (pp, uu) = solf.split()
    filep << pp
    fileu << uu
    np.savetxt(outdir + "exu.txt", Fobs, fmt='%g')

    # ----- INIT -----
    # random field
    np.random.seed(973453)
    RRCur = np.zeros(klecount)
    RR = np.zeros(klecount)
    for ki in range(klecount):
        RR[ki] = np.random.normal(0.0, 1.0)
    # generate (k,E)
    theta = generateKLE(RR)
    arrK = generateK(theta)
    arrE = generateE(theta)
    E = HeterExpression(degree = 0)
    E.setArr(dim, Nx, Ny, Nz, arrE)
    k = HeterExpression(degree = 0)
    k.setArr(dim, Nx, Ny, Nz, arrK)
    # solve fine
    problemF.form2mat(c, dt, k, E, nu, alpha)
    solf = solveProblem(False, problemF, problemF, k, E, nu, c, dt, alpha); solf.rename('u', '0')  
    Fprop = findF(solf)
    errut = 0.0; sumut = 0.0
    for i in range(len(Fobs)):
        errut += math.pow(Fprop[i] - Fobs[i], 2)
        sumut += math.pow(Fobs[i], 2)
    prevNormF = errut/sumut
    # solve ms
    solms = solveProblem(True, problemF, problemMs, k, E, nu, c, dt, alpha); solms.rename('u', '0') 
    Fprop = findF(solms)
    errut = 0.0; sumut = 0.0
    for i in range(len(Fobs)):
        errut += math.pow(Fprop[i] - Fobs[i], 2)
        sumut += math.pow(Fobs[i], 2)
    prevNormMs = errut/sumut        
    print('---> INIT prevNormF = %g, ERROR_TOP_F = %g  (prevNormMs = %g, err = %g)' 
                                % (prevNormF, math.sqrt(prevNormF)*100, prevNormMs, math.sqrt(prevNormMs)*100) )        
            
    # ----- MCMC -----
    timerGl.start()
    stage1mi = []
    stage2mi = []
    allFnorms = np.zeros((mnum, 3))
    for mi in range(mnum):
        print('----- %d -----' % mi)
        # eandom walk
        for ki in range(klecount):
            rr = np.random.normal(0.0, 1.0)
            RRCur[ki] = ( RR[ki] + delta*rr )/math.sqrt(1.0 + delta**2)
        # current field 
        theta = generateKLE(RRCur)
        arrK = generateK(theta)
        arrE = generateE(theta)
        E = HeterExpression(degree = 0)
        E.setArr(dim, Nx, Ny, Nz, arrE)
        k = HeterExpression(degree = 0)
        k.setArr(dim, Nx, Ny, Nz, arrK)

        # solve ms
        problemF.form2mat(c, dt, k, E, nu, alpha)
        solms = solveProblem(True, problemF, problemMs, k, E, nu, c, dt, alpha); solms.rename('u', '0') 
        Fprop = findF(solms)
        errut = 0.0; sumut = 0.0
        for i in range(len(Fobs)):
            errut += math.pow(Fprop[i] - Fobs[i], 2)
            sumut += math.pow(Fobs[i], 2)
        curNormMs = errut/sumut
        allFnorms[mi, 0] = curNormMs
        print('---> curNormMs = %g, ERROR_TOP_MS = %g' % (curNormMs, math.sqrt(curNormMs)*100) )   
       
        iscuraccepted = False
        # check ms
        xval1 = -(curNormMs - prevNormMs)/sc2
        xval = min(xval1, 10)
        alphaMs = math.exp(xval)
        # alphaMs = math.exp(-curNormMs/sc2) / math.exp(-prevNorm/sc2) 
        probMs = min(1.0, alphaMs)
        rn = random()
        isacceptMs = (rn < probMs)
        print('---> STAGE 1 (MS): r = %g, prob = %g (alpha %g with %g->%g), isAccepted = %s' % (rn, probMs, alphaMs, xval1, xval, isacceptMs))
        if isacceptMs:
            stage1mi.append(mi)
            # solve fine
            solf = solveProblem(False, problemF, problemF, k, E, nu, c, dt, alpha); solf.rename('u', '0')  
            Fprop = findF(solf)
            errut = 0.0; sumut = 0.0
            for i in range(len(Fobs)):
                errut += math.pow(Fprop[i] - Fobs[i], 2)
                sumut += math.pow(Fobs[i], 2)
            curNormF = errut/sumut
            allFnorms[mi, 1] = curNormF
            print('------> curNormF = %g, ERROR_TOP_F = %g' % (curNormF, math.sqrt(curNormF)*100) )    
            
            # check fine
            xval1 = -  ( (curNormF - prevNormF)/sf2 - (curNormMs - prevNormMs)/sc2 )
            xval = min(xval1, 10)
            alphaF = math.exp(xval)
            # pp1 = math.exp(-curNormF/sf2) / math.exp(-prevNorm/sf2) 
            # pp2 =  math.exp(-normMs/sc2) / math.exp(-prevNorm/sc2) 
            # alphaF = pp1/pp2
            probF = min(1.0, alphaF)
            rn = random()
            isacceptF = (rn < probF)
            print('------> STAGE 2 (FINE): r = %g, prob = %g (alpha %g with %g->%g), isAccepted = %s ' % (rn, probF, alphaF, xval1, xval, isacceptF))
            if isacceptF:
                stage2mi.append(mi)
                prevNormF = curNormF
                prevNormMs = curNormMs
                iscuraccepted = True
                allFnorms[mi, 2] = 1
                # keep current RR
                for ki in range(klecount):
                    RR[ki] = RRCur[ki] 
                print("====== ACCEPTED %d, update RR (%g, %g) ======" % (mi, min(RR), max(RR)))
                # error
                calcerror(np.array(solms.vector()), np.array(solf.vector()), Nvert)
                # save
                u0.interpolate(k); fileKA << u0
                u0.interpolate(E); fileEA << u0
                (pp, uu) = solf.split()
                filepA << pp
                fileuA << uu
                np.savetxt(outdir + str(len(stage2mi))+".txt", Fprop, fmt='%g')
                
        if not iscuraccepted:
            # keep previous RR
            print("====== NOT ACCEPTED %d, keep RR (%g, %g) ======" % (mi, min(RR), max(RR)))
    print("DONE, time = %g sec" % timerGl.stop())

    print(stage1mi)
    print(stage2mi)
    print(len(stage1mi), len(stage2mi))
     # save
    np.savetxt(outdir + "t3-mcmc-F.txt", allFnorms, fmt='%g')

# ===========================
# ===== ONE-STAGE MCMC ======
# ===========================
if mtype == 4:
    # params
    sf2 = math.pow(sf, 2)# 0.05

    # accepted
    fileKA = File(outdir + "kA.pvd")
    fileEA = File(outdir + "EA.pvd")
    filepA = File(outdir + "pA.pvd")
    fileuA = File(outdir + "uA.pvd")

    # save exact
    u0.interpolate(k); fileK << u0
    u0.interpolate(E); fileE << u0
    (pp, uu) = solf.split()
    filep << pp
    fileu << uu
    np.savetxt(outdir + "exu.txt", Fobs, fmt='%g')

    # ----- INIT -----
    # random field
    np.random.seed(973453)
    RRCur = np.zeros(klecount)
    RR = np.zeros(klecount)
    for ki in range(klecount):
        RR[ki] = np.random.normal(0.0, 1.0)
    # generate (k,E)
    theta = generateKLE(RR)
    arrK = generateK(theta)
    arrE = generateE(theta)
    E = HeterExpression(degree = 0)
    E.setArr(dim, Nx, Ny, Nz, arrE)
    k = HeterExpression(degree = 0)
    k.setArr(dim, Nx, Ny, Nz, arrK)
    # solve fine
    problemF.form2mat(c, dt, k, E, nu, alpha)
    solf = solveProblem(False, problemF, problemF, k, E, nu, c, dt, alpha); solf.rename('u', '0')  
    Fprop = findF(solf)
    errut = 0.0; sumut = 0.0
    for i in range(len(Fobs)):
        errut += math.pow(Fprop[i] - Fobs[i], 2)
        sumut += math.pow(Fobs[i], 2)
    prevNorm = errut/sumut
    print('---> INIT normF = %g, ERROR_TOP_F = %g' % (prevNorm, math.sqrt(prevNorm)*100) )        
            
    # ----- MCMC -----
    timerGl.start()
    stage1mi = []
    allFnorms = np.zeros((mnum, 3))
    for mi in range(mnum):
        print('----- %d -----' % mi)
        # eandom walk
        for ki in range(klecount):
            rr = np.random.normal(0.0, 1.0)
            RRCur[ki] = ( RR[ki] + delta*rr )/math.sqrt(1.0 + delta**2)
        # current field 
        theta = generateKLE(RRCur)
        arrK = generateK(theta)
        arrE = generateE(theta)
        E = HeterExpression(degree = 0)
        E.setArr(dim, Nx, Ny, Nz, arrE)
        k = HeterExpression(degree = 0)
        k.setArr(dim, Nx, Ny, Nz, arrK)

        # solve fine
        problemF.form2mat(c, dt, k, E, nu, alpha)
        solf = solveProblem(False, problemF, problemF, k, E, nu, c, dt, alpha); solf.rename('u', '0')  
        Fprop = findF(solf)
        errut = 0.0; sumut = 0.0
        for i in range(len(Fobs)):
            errut += math.pow(Fprop[i] - Fobs[i], 2)
            sumut += math.pow(Fobs[i], 2)
        curNorm = errut/sumut
        allFnorms[mi, 0] = curNorm
        print('---> curNorm = %g, ERROR_TOP_MS = %g' % (curNorm, math.sqrt(curNorm)*100) )   
        

        iscuraccepted = False
        # check fine
        xval1 = -(curNorm - prevNorm)/sf2
        xval = min(xval1, 10)
        alphaF = math.exp(xval) 
        prob = min(1.0, alphaF)
        rn = random()
        isaccept = (rn < prob)
        print('---> STAGE 1 (Fine): r = %g, prob = %g (alpha=%g, %g, %g), isAccepted = %s' % (rn, prob, alphaF, xval1, xval, isaccept))
        if isaccept:
            stage1mi.append(mi)
            prevNorm = curNorm
            iscuraccepted = True
            allFnorms[mi, 1] = 1
            # keep current RR
            for ki in range(klecount):
                RR[ki] = RRCur[ki] 
            print("====== ACCEPTED %d, update RR (%g, %g) ======" % (mi, min(RR), max(RR)))
            # save
            u0.interpolate(k); fileKA << u0
            u0.interpolate(E); fileEA << u0
            (pp, uu) = solf.split()
            filepA << pp
            fileuA << uu
            np.savetxt(outdir + str(len(stage1mi)) + ".txt", Fprop, fmt='%g')
                
        if not iscuraccepted:
            # keep previous RR
            print("====== NOT ACCEPTED %d, keep RR (%g, %g) ======" % (mi, min(RR), max(RR)))
    print("DONE, time = %g sec" % timerGl.stop())

    print(stage1mi)
    print( len(stage1mi) )
     # save
    np.savetxt(outdir + "t4-mcmc-F.txt", allFnorms, fmt='%g')

# ===========================
# ======== KERAS ML =========
# ===== TWO-STAGE MCMC ======
# ===========================
if mtype == 5:
    # params 
    sf2 = math.pow(sf, 2)
    sc2 = sf2 * beta

    # load ml
    from keras import backend as K
    from keras.optimizers import *
    from keras.models import Model, load_model
    def rmse(y_true, y_pred):
        return K.sqrt(K.mean(K.square(y_pred - y_true), axis=-1))/K.sqrt(K.mean(K.square(y_true), axis=-1))*100.0
    def mae2(y_true, y_pred):
        return K.sum(K.abs(y_pred - y_true), axis=-1)/K.sum(K.abs(y_true), axis=-1)*100.0

    xmin = minphi; xmax = maxphi 
    if dim == 2:
        netname = './ml/model2d'
        vmin = [-0.00671568, -0.0477921]
        vmax = [ 0.0223867,  -0.0088949 ]
    if dim == 3:
        netname = './ml/model3d'
        vmin = [-0.00958047, -0.0486761, -0.00644912]  
        vmax = [ 0.0171148,  -0.00976819, 0.0183037]
    models = []
    model1 = load_model(netname + '-0.net', custom_objects={'rmse': rmse, 'mae2':mae2})
    models.append(model1)
    model2 = load_model(netname + '-1.net', custom_objects={'rmse': rmse, 'mae2':mae2})
    models.append(model2)
    if dim == 3:
        model3 = load_model(netname + '-2.net', custom_objects={'rmse': rmse, 'mae2':mae2})
        models.append(model3)
    print('load ML-models')
    if dim == 2:
        FpropMl = np.zeros( (Nx+1)*dim )
    if dim == 3:
        FpropMl = np.zeros( (Nx+1)*(Nz+1)*dim )

    # accepted files
    fileKA = File(outdir + "kA.pvd")
    fileEA = File(outdir + "EA.pvd")
    filepA = File(outdir + "pA.pvd")
    fileuA = File(outdir + "uA.pvd")

    # save exact
    u0.interpolate(k); fileK << u0
    u0.interpolate(E); fileE << u0
    (pp, uu) = solf.split()
    filep << pp
    fileu << uu
    np.savetxt(outdir + "exu.txt", Fobs, fmt='%g')

    # ----- INIT -----
    # random field
    np.random.seed(973453)
    RRCur = np.zeros(klecount)
    RR = np.zeros(klecount)
    for ki in range(klecount):
        RR[ki] = np.random.normal(0.0, 1.0)
    # generate (k,E)
    theta = generateKLE(RR)
    arrK = generateK(theta)
    arrE = generateE(theta)
    E = HeterExpression(degree = 0)
    E.setArr(dim, Nx, Ny, Nz, arrE)
    k = HeterExpression(degree = 0)
    k.setArr(dim, Nx, Ny, Nz, arrK)
    # solve fine
    problemF.form2mat(c, dt, k, E, nu, alpha)
    solf = solveProblem(False, problemF, problemF, k, E, nu, c, dt, alpha); solf.rename('u', '0')  
    Fprop = findF(solf)
    errut = 0.0; sumut = 0.0
    for i in range(len(Fobs)):
        errut += math.pow(Fprop[i] - Fobs[i], 2)
        sumut += math.pow(Fobs[i], 2)
    prevNormF = errut/sumut
    prevNormMs = prevNormF
    # solve ms
    # input ML
    X_train1 = (theta[:] - xmin) / (xmax - xmin)
    if dim == 2:
        X_train1 = X_train1.reshape(1, Nx, Ny, 1)
    if dim == 3:
        X_train1 = X_train1.reshape(1, Nx, Ny, Nz, 1)
    # predict ML
    for tj in range(dim):
        probs = models[tj].predict(X_train1)
        Fft = probs[0]*(vmax[tj] - vmin[tj]) + vmin[tj]
        # fill Fobs
        if dim == 2:
            for ii in range(Nx+1):
                FpropMl[dim*ii + tj] = Fft[ii]
        if dim == 3:
            for ii in range(Nx+1):
                for jj in range(Nz+1):
                    ind = jj*(Nx+1) + ii
                    FpropMl[dim*ind + tj] = Fft[ind]

    # error
    errut = 0.0; sumut = 0.0
    for i in range(len(Fobs)):
        errut += math.pow(FpropMl[i] - Fobs[i], 2)
        sumut += math.pow(Fobs[i], 2)
    prevNormMs = errut/sumut        
    print('---> INIT prevNormF = %g, ERROR_TOP_F = %g  (prevNormMs = %g, err = %g)' 
                                % (prevNormF, math.sqrt(prevNormF)*100, prevNormMs, math.sqrt(prevNormMs)*100) )        
            
    # ----- MCMC -----
    timerGl.start()
    stage1mi = []
    stage2mi = []
    allFnorms = np.zeros((mnum, 3))
    for mi in range(mnum):
        print('----- %d -----' % mi)
        # random walk
        timer.start()
        for ki in range(klecount):
            rr = np.random.normal(0.0, 1.0)
            RRCur[ki] = ( RR[ki] + delta*rr )/math.sqrt(1.0 + delta**2)
        # current field 
        print('---> TIME0 = %g sec' % timer.stop() ) 
        timer.start()
        theta = generateKLE(RRCur)
        print('---> TIME1 = %g sec' % timer.stop() ) 
        
        # input ML
        timer.start()
        X_train1 = (theta[:] - xmin) / (xmax - xmin)
        if dim == 2:
            X_train1 = X_train1.reshape(1, Nx, Ny, 1)
        if dim == 3:
            X_train1 = X_train1.reshape(1, Nx, Ny, Nz, 1)
        # predict ML
        for tj in range(dim):
            probs = models[tj].predict(X_train1)
            Fft = probs[0]*(vmax[tj] - vmin[tj]) + vmin[tj]
            # fill Fobs
            if dim == 2:
                for ii in range(Nx+1):
                    FpropMl[dim*ii + tj] = Fft[ii]
            if dim == 3:
                for ii in range(Nx+1):
                    for jj in range(Nz+1):
                        ind = jj*(Nx+1) + ii
                        FpropMl[dim*ind + tj] = Fft[ind]
        # error
        errut = 0.0; sumut = 0.0
        for i in range(len(Fobs)):
            errut += math.pow(FpropMl[i] - Fobs[i], 2)
            sumut += math.pow(Fobs[i], 2)
        curNormMs = errut/sumut
        allFnorms[mi, 0] = curNormMs
        print('---> curNormML = %g, ERROR_TOP_ML = %g, TIME_ML = %g sec' % (curNormMs, math.sqrt(curNormMs)*100, timer.stop()) )   
       
        iscuraccepted = False
        # check ML
        xval1 = -(curNormMs - prevNormMs)/sc2
        xval = min(xval1, 10)
        alphaMs = math.exp(xval)
        # alphaMs = math.exp(-curNormMs/sc2) / math.exp(-prevNorm/sc2) 
        probMs = min(1.0, alphaMs)
        rn = random()
        isacceptMs = (rn < probMs)
        print('---> STAGE 1 (MS): r = %g, prob = %g (alpha %g with %g->%g), isAccepted = %s' % (rn, probMs, alphaMs, xval1, xval, isacceptMs))
        if isacceptMs:
            stage1mi.append(mi)

            arrK = generateK(theta)
            arrE = generateE(theta)
            E = HeterExpression(degree = 0)
            E.setArr(dim, Nx, Ny, Nz, arrE)
            k = HeterExpression(degree = 0)
            k.setArr(dim, Nx, Ny, Nz, arrK)

            # solve fine
            problemF.form2mat(c, dt, k, E, nu, alpha)
            solf = solveProblem(False, problemF, problemF, k, E, nu, c, dt, alpha); solf.rename('u', '0')  
            Fprop = findF(solf)
            errut = 0.0; sumut = 0.0
            for i in range(len(Fobs)):
                errut += math.pow(Fprop[i] - Fobs[i], 2)
                sumut += math.pow(Fobs[i], 2)
            curNormF = errut/sumut
            allFnorms[mi, 1] = curNormF
            print('------> curNormF = %g, ERROR_TOP_F = %g' % (curNormF, math.sqrt(curNormF)*100) )    
            
            # check fine
            xval1 = -  ( (curNormF - prevNormF)/sf2 - (curNormMs - prevNormMs)/sc2 )
            xval = min(xval1, 10)
            alphaF = math.exp(xval)
            # pp1 = math.exp(-curNormF/sf2) / math.exp(-prevNorm/sf2) 
            # pp2 =  math.exp(-normMs/sc2) / math.exp(-prevNorm/sc2) 
            # alphaF = pp1/pp2
            probF = min(1.0, alphaF)
            rn = random()
            isacceptF = (rn < probF)
            print('------> STAGE 2 (FINE): r = %g, prob = %g (alpha %g with %g->%g), isAccepted = %s ' % (rn, probF, alphaF, xval1, xval, isacceptF))
            if isacceptF:
                stage2mi.append(mi)
                prevNormF = curNormF
                prevNormMs = curNormMs
                iscuraccepted = True
                allFnorms[mi, 2] = 1
                # keep current RR
                for ki in range(klecount):
                    RR[ki] = RRCur[ki] 
                print("====== ACCEPTED %d, update RR (%g, %g) ======" % (mi, min(RR), max(RR)))
                # error
                errut = 0.0; sumut = 0.0
                for i in range(len(Fprop)):
                    errut += math.pow(FpropMl[i] - Fprop[i], 2)
                    sumut += math.pow(Fprop[i], 2)
                print('-----> ERROR ML: ERROR_TOP_F = %g' % ( math.sqrt(errut/sumut)*100 ) ) 
                # save
                u0.interpolate(k); fileKA << u0
                u0.interpolate(E); fileEA << u0
                (pp, uu) = solf.split()
                filepA << pp
                fileuA << uu
                np.savetxt(outdir + str(len(stage2mi))+".txt", Fprop, fmt='%g')
                
        if not iscuraccepted:
            # keep previous RR
            print("====== NOT ACCEPTED %d, keep RR (%g, %g) ======" % (mi, min(RR), max(RR)))
    print("DONE, time = %g sec" % timerGl.stop())

    print(stage1mi)
    print(stage2mi)
    print(len(stage1mi), len(stage2mi))
     # save
    np.savetxt(outdir + "t3-mcmc-F.txt", allFnorms, fmt='%g')

print("DONE")